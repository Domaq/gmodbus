﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        dynamic ax;
        string previousMessage;
        string device = "\\\\.\\COM1";
        int servID = 17;
        int baud = 9600;

        public Form1()
        {
            InitializeComponent();
            ax = this.axGModbus1.GetOcx();

            device = "\\\\.\\COM1";
            servID = 17;
            baud = 9600;

            if (ax != null)
            {
                string str = ax.getConnectionStatusInfo();
                richTextBox1.Text = str;
                previousMessage = str;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ax != null)
            {
                string ip = "192.168.0.102";
                int port = 1502;
                ax.modbusReadRegistersFromRemoteSlaveTCP(ip, port, 0,6);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ax != null)
            {
                string 	ip = "192.168.10.57";
	            int port = 3487;
                ax.initConnectionAsSlave(ip,port);
                ax.modbusRemoteMastersInitialization(1);
                ax.modbusAddMasterToRemoteSlaveRTU(device, servID, baud);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (ax != null)
            {
                ax.stopSlave();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int k = 0;
            if (ax != null)
            {
                bool stop = false;
                for (int i = 0; i < 1000; i++)
                {
                    for (k = 0; k < 10; k++)
                    {
                        if (!ax.modbusReadRegistersFromRemoteSlave(k, 6,0))
                        {
                            stop = true;
                            break;
                        }
                    }
                    if (stop)
                    {
                        if(!ax.modbusMastersUpdateConections())
                            break;
                    }
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (ax != null)
            {
                /*
                float[] estimateParams = new float[3];
                for (int i = 0; i < 3; ++i)
                {
                    estimateParams[i] = (float)(i + 0.5);
                }
                 //*/
                
                int[] estimateParams = new int[3];
                for (int i = 0; i < 3; ++i)
                {
                    estimateParams[i] = i;
                }
                //*/
                for(; ; )
                {
                    ax.modbusWriteRegistersToServer(0, 6, 16, 0, estimateParams);
                }
            }


        }
    }
}
