#include "stdafx.h"
#include "ModbusAdapter.h"

ModbusAdapterData::ModbusAdapterData() : tp(DataType::NODATATYPE), registers(NULL), bits(NULL), flvals(NULL), lvals(NULL) {}
ModbusAdapterData::ModbusAdapterData(uint16_t* registers) : tp(DataType::REGISTRS), bits(NULL), flvals(NULL), lvals(NULL)  { this->registers = registers; }
ModbusAdapterData::ModbusAdapterData(uint8_t* bits) : tp(DataType::BITS), registers(NULL), flvals(NULL), lvals(NULL)  { this->bits = bits; }
ModbusAdapterData::ModbusAdapterData(float* flvals) : tp(DataType::FLOATS), registers(NULL), bits(NULL), lvals(NULL) { this->flvals = flvals; }
ModbusAdapterData::ModbusAdapterData(long* lvals) : tp(DataType::LONGS), registers(NULL), bits(NULL), flvals(NULL) { this->lvals = lvals; }
ModbusAdapterData::ModbusAdapterData(int nb, DataType dp) : registers(NULL), bits(NULL), flvals(NULL), lvals(NULL)
{
	this->tp = dp;
	switch (dp)
	{
	case DataType::REGISTRS:
	{
		this->registers = new uint16_t[nb];
		break;
	}
	case DataType::BITS:
	{
		this->bits = new uint8_t[nb];
		break;
	}
	case DataType::FLOATS:
	{
		this->flvals = new float[nb];
		break;
	}
	case DataType::LONGS:
	{
		this->lvals = new long[nb];
		break;
	}
	default:
		break;
	}
}
ModbusAdapterData::~ModbusAdapterData()
{
	if (registers != NULL)
		delete[] registers;

	if (bits != NULL)
		delete[] bits;

	if (flvals != NULL)
		delete[] flvals;

	if (lvals != NULL)
		delete[] lvals;
}

bool ModbusAdapterData::setRegisters(uint16_t* registers)
{
	if (this->tp == DataType::NODATATYPE)
	{
		this->registers = registers;
		return true;
	}
	return false;
}
bool ModbusAdapterData::setBits(uint8_t* bits)
{
	if (this->tp == DataType::NODATATYPE)
	{
		this->bits = bits;
		return true;
	}
	return false;
}
bool ModbusAdapterData::setFloats(float* flvals)
{
	if (this->tp == DataType::NODATATYPE)
	{
		this->flvals = flvals;
		return true;
	}
	return false;
}

bool ModbusAdapterData::setLongs(long* lvals)
{
	if (this->tp == DataType::NODATATYPE)
	{
		this->lvals = lvals;
		return true;
	}
	return false;
}

//* MODBUSADAPTER *//
ModBusAdapter::ModBusAdapter()
{
	this->master_ctx = NULL;
	this->masterConnectionStatus = false;
}
ModBusAdapter::~ModBusAdapter()
{
	modbus_flush(master_ctx);
	modbus_close(master_ctx);
	modbus_free(master_ctx);
	master_ctx = NULL;
}

//* MPDBUS ADAPTER SLAVE *//
void ModBusAdapter::modbusSlaveStart(bool & stopflag, std::string &err, MasterConnectionParams* &mcp)
{
	//AfxMessageBox(_T("ModBusAdapter:: � ����� ������. ������� ������� Slave"));
	uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
	int master_socket;
	int rc;
	fd_set refset;
	fd_set rdset;
	/* Maximum file descriptor number */
	int fdmax;

	/*debug
	CStringA message1("Slave. \nip: ");
	const char* result1 = mcp->ip.c_str();
	CStringA tmp1(result1);

	CStringA message2("\nport: ");
	std::string portsrt = std::to_string(mcp->port);
	const char* result2 = portsrt.c_str();
	CStringA tmp2(result2);

	CStringA message = message1 + tmp1 + message2 + tmp2;
	AfxMessageBox(message.AllocSysString());
	/*debug*/


	ctx = modbus_new_tcp(mcp->ip.c_str(), mcp->port);
	//modbus_set_debug(ctx, TRUE);

	server_socket = modbus_tcp_listen(ctx, NB_CONNECTION + 1);
	if (server_socket == -1)
	{
		AfxMessageBox(_T("ModBusAdapter:: Unable to listen TCP connection \n"));
		err += "ModBusAdapter:: Unable to listen TCP connection \n";
		modbus_free(ctx);
		return;
	}

	signal(SIGINT, close_sigint);

	/* Clear the reference set of socket */
	FD_ZERO(&refset);
	/* Add the server socket */
	FD_SET(server_socket, &refset);

	/* Keep track of the max file descriptor */
	fdmax = server_socket;

	stopflag = true;
	for (;;)
	{

		if (stopflag == false)
		{
			close_sigint(1);
			break;
		}

		rdset = refset;
		if (select(fdmax + 1, &rdset, NULL, NULL, NULL) == -1)
		{
			AfxMessageBox(_T("ModBusAdapter:: Server select() failure. \n"));
			err += "ModBusAdapter:: Server select() failure. \n";
			perror("Server select() failure.");
			close_sigint(1);
		}
		/* Run through the existing connections looking for data to be
		* read */
		for (master_socket = 0; master_socket <= fdmax; master_socket++)
		{
			if (!FD_ISSET(master_socket, &rdset))
			{
				continue;
			}

			if (master_socket == server_socket)
			{

				/* A client is asking a new connection */
				socklen_t addrlen;
				struct sockaddr_in clientaddr;
				int newfd;

				/* Handle new connections */
				addrlen = sizeof(clientaddr);
				memset(&clientaddr, 0, sizeof(clientaddr));
				newfd = accept(server_socket, (struct sockaddr *)&clientaddr, &addrlen);
				if (newfd == -1)
				{
					err += "ModBusAdapter:: Server accept() error \n";
					perror("Server accept() error");
				}
				else
				{
					FD_SET(newfd, &refset);

					if (newfd > fdmax)
					{
						/* Keep track of the maximum */
						fdmax = newfd;
					}
					/*debug*/
					//AfxMessageBox(_T("NEW CONNECTION"));
					/*debug*/

					//err += "ModBusAdapter:: New connection \n";
				}
			}
			else
			{
				modbus_set_socket(ctx, master_socket);
				rc = modbus_receive(ctx, query);
				if (rc > 0)
				{
					modbus_reply(ctx, query, rc, mb_mapping);
				}
				else if (rc == -1)
				{
					/* This example server in ended on connection closing or
					* any errors. */
					err += "ModBusAdapter:: Connection closed \n";
					//std::cout << "Connection closed on socket " << master_socket << std::endl;
					closesocket(master_socket);

					/* Remove from reference set */
					FD_CLR(master_socket, &refset);

					if (master_socket == fdmax)
					{
						fdmax--;
					}
				}
			}
		}

#ifdef  ONE_THREAD_MODE
		close_sigint(1);
		break;
#endif
	}
}

//* MPDBUS ADAPTER MASTER *//

int ModBusAdapter::makeConnection(std::string &err, MasterConnectionParams* &mcp)
{

	// ������� ������ ���� ������ �� ���������������
	if ((master_ctx != NULL) && (this->masterConnectionStatus != true))
	{		
		// ��������� ���������� � ������� �����������
		err += "ModBusAdapter:: master_ctx != NULL \n";
		return _RC_NOT_NULL_ERROR_;
	}
	//AfxMessageBox(_T("1"));
	switch (mcp->tp)
	{
		case ConnectionType::TCP:
		{
			this->master_ctx = modbus_new_tcp(mcp->ip.c_str(), mcp->port);
			//AfxMessageBox(_T("TCP"));
			break;
		}
		case ConnectionType::RTU:
		{
			this->master_ctx = modbus_new_rtu(mcp->device.c_str(), mcp->baud, mcp->parity, mcp->data_bit, mcp->stop_bit);
			//AfxMessageBox(_T("RTU"));
			break;
		}
		default:
		{
			err += "ModBusAdapter:: Wrong connection type \n";
			return _RC_INCORRECT_INPUT_DATA_ERROR_;
		}
	}
	if (this->master_ctx == NULL)
	{
		AfxMessageBox(_T("ModBusAdapter::Unable to allocate libmodbus context \n"));
		err += "ModBusAdapter::Unable to allocate libmodbus context \n";
		return _RC_CONNECTION_ERROR;
	}
	//AfxMessageBox(_T("2"));
	//modbus_set_debug(this->master_ctx, TRUE);
	//if (mcp->tp == ConnectionType::RTU)
	//{
	modbus_set_slave(this->master_ctx, mcp->slaveId); // � ������ RTU ����������� ���������� ������������������ ����� ��������� (��������� slave)
	//}
	/*CString msgstr;
	msgstr.Format(_T("slave id = %d"), mcp->slaveId);
	AfxMessageBox(msgstr);//*/
	if (modbus_connect(master_ctx) == -1)
	{
		//AfxMessageBox(_T("ModBusAdapter::makeConnection: connection failed \n"));
		//err += "ModBusAdapter::makeConnection: connection failed \n";
		//modbus_free(master_ctx);
		//master_ctx = NULL;
		return _RC_CONNECTION_ERROR;
	}
	//AfxMessageBox(_T("3"));
	this->masterConnectionStatus = true;
	return _RC_SUCCES_;
}
int ModBusAdapter::modbusInteractionWithSlave(int addr, int nb, ModbusAdapterData* data, FunctionType ft,std::string &err)
{
	if (data->getDataType() == DataType::BITS)
	{
		return this->performWithBitsFunctions(addr, nb, data->getBits(), ft, err);
	}
	if (data->getDataType() == DataType::REGISTRS)
	{
		return this->performWithRegistersFunctions(addr, nb, data->getRegisters(), ft, err);
	}
	err += "Wrong Input data";
	return _RC_INCORRECT_INPUT_DATA_ERROR_;
}
int ModBusAdapter::performWithRegistersFunctions(int addr, int nb, uint16_t* result, FunctionType ft,std::string &err)
{
	int rc = _RC_UNKNOWN_ERROR_;
	// ������� ��������������� ������� modbuslib
	switch (ft)
	{
	case FunctionType::Read_Holding_Registers:
	{
		//AfxMessageBox(_T("!!"));
		Sleep(100);
		rc = modbus_read_registers(this->master_ctx, addr, nb, result);
		break;
	}
	case FunctionType::Read_Input_Registers:
	{
		rc = modbus_read_input_registers(this->master_ctx, addr, nb, result);
		break;
	}
	case FunctionType::Write_Multiple_Registers:
	{
		rc = modbus_write_registers(this->master_ctx, addr, nb, result);
		break;
	}
	default:
		err += "Wrong function type \n";
		return _RC_INCORRECT_FUNCTION_TYPE_ERROR_;
	}
	if (rc == _RC_UNKNOWN_ERROR_)
	{
		// ���� �� ���������, �������� ���������� �� ������
		err += modbus_strerror(errno);
		return rc;
	}
	return _RC_SUCCES_;
}
int ModBusAdapter::performWithBitsFunctions(int addr, int nb, uint8_t* result, FunctionType ft, std::string &err)
{
	int rc = _RC_UNKNOWN_ERROR_;
	// ������� ��������������� ������� modbuslib
	switch (ft)
	{
	case FunctionType::Read_Coil_Status:
	{
		rc = modbus_read_bits(this->master_ctx, addr, nb, result);
		break;
	}
	case FunctionType::Read_Discrete_Inputs:
	{
		rc = modbus_read_input_bits(this->master_ctx, addr, nb, result);
		break;
	}
	case FunctionType::Write_Multiple_Coils:
	{
		rc = modbus_write_bits(this->master_ctx, addr, nb, result);
		break;
	}
	default:
		err += "Wrong function type \n";
		return _RC_INCORRECT_FUNCTION_TYPE_ERROR_;
	}
	if (rc == _RC_UNKNOWN_ERROR_)
	{
		// ���� �� ���������, �������� ���������� �� ������
		err += modbus_strerror(errno);
		return rc;
	}
	return _RC_SUCCES_;
}

