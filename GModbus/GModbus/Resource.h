//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GModbus.rc
//

#define IDS_GMODBUS               1
#define IDS_GMODBUS_PPG           2

#define IDS_GMODBUS_PPG_CAPTION   200

#define IDD_PROPPAGE_GMODBUS      200

#define IDD_ABOUTBOX_GMODBUS      1

#define IDB_GMODBUS               1

#define IDI_ABOUTDLL				1

#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           101
#define _APS_NEXT_COMMAND_VALUE         32768
