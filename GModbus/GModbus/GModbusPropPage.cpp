// GModbusPropPage.cpp: ���������� ������ �������� ������� CGModbusPropPage.

#include "stdafx.h"
#include "GModbus.h"
#include "GModbusPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CGModbusPropPage, COlePropertyPage)

// ����� ���������

BEGIN_MESSAGE_MAP(CGModbusPropPage, COlePropertyPage)
END_MESSAGE_MAP()

// ���������������� ������� ������ � guid

IMPLEMENT_OLECREATE_EX(CGModbusPropPage, "GMODBUS.GModbusPropPage.1",
	0xc2a6eb48, 0x433e, 0x431c, 0x94, 0x56, 0xb0, 0x6e, 0xa6, 0xf9, 0xfe, 0x5b)

// CGModbusPropPage::CGModbusPropPageFactory::UpdateRegistry -
// ���������� ��� �������� ������� ���������� ������� ��� CGModbusPropPage

BOOL CGModbusPropPage::CGModbusPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_GMODBUS_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}

// CGModbusPropPage::CGModbusPropPage - �����������

CGModbusPropPage::CGModbusPropPage() :
	COlePropertyPage(IDD, IDS_GMODBUS_PPG_CAPTION)
{
}

// CGModbusPropPage::DoDataExchange - ������� ������ ����� ��������� � ����������

void CGModbusPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}

// ����������� ��������� CGModbusPropPage
