#pragma once

// GModbusPropPage.h: ���������� ������ �������� ������� CGModbusPropPage.


// CGModbusPropPage: ��� ���������� ��. GModbusPropPage.cpp.

class CGModbusPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CGModbusPropPage)
	DECLARE_OLECREATE_EX(CGModbusPropPage)

// �����������
public:
	CGModbusPropPage();

// ������ ����������� ����
	enum { IDD = IDD_PROPPAGE_GMODBUS };

// ����������
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����� ���������
protected:
	DECLARE_MESSAGE_MAP()
};

