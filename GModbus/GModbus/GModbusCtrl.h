#pragma once

// GModbusCtrl.h: ���������� ������ �������� ���������� ActiveX ��� CGModbusCtrl.

// CGModbusCtrl: ��� ���������� ��. GModbusCtrl.cpp.
#include "ModbusAdapter.h"
//#define MODBUS_MASTERS_FOR_REMOTE_SLAVES_NUMBER 1

class CGModbusCtrl : public COleControl
{
	DECLARE_DYNCREATE(CGModbusCtrl)

// �����������
public:
	CGModbusCtrl();

// ���������������
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// ����������
protected:
	~CGModbusCtrl();

	DECLARE_OLECREATE_EX(CGModbusCtrl)    // ������� ������ � guid
	DECLARE_OLETYPELIB(CGModbusCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CGModbusCtrl)     // �� �������� �������
	DECLARE_OLECTLTYPE(CGModbusCtrl)	  // ������� ��� � ������������� ���������

// ����� ���������
	DECLARE_MESSAGE_MAP()

// ����� ���������� � ��������
	DECLARE_DISPATCH_MAP()
	
	//* INFO *//
	afx_msg void AboutBox();
	afx_msg BSTR getConnectionStatusInfo();
	void showErrorMsgBox(std::string error);

	//* MPDBUS SLAVE *//
	// �������������� ����� ���������
	int createMap(int start_addr);
	// �������� �������, ������������ ��������� slave
	static UINT initConnectionAsSlaveThreadFunction(LPVOID pParam);
	// �������� initConnectionAsSlaveThreadFunction + �������������� ���������� �������
	afx_msg int initConnectionAsSlave(LPCTSTR ip, int port);
	// ������������� ��������� slave � ������� ���� ��������
	afx_msg void stopSlave();

	//* MPDBUS MASTER *//
	// ������ nb ��������� �� ������ addr � ��������� salve, ������ �������� rmtSlaveIndex. ��������� ���������� � ����� ����� ��������� slave
	afx_msg int modbusReadRegistersFromRemoteSlave(int addr, int nb, int rmtSlaveIndex);
	// ���������� nb ��������� �� ������ addr � ��������� slave, ������ �������� rmtSlaveIndex. ������ ���� �� ����� ����� ��������� slave
	afx_msg int modbusWriteRegistersToRemoteSlave(int addr, int nb, int rmtSlaveIndex);
	// 
	afx_msg int modbusWriteToServer(int addr, int nb, int ft, int bo, const VARIANT &Data);
	afx_msg VARIANT modbusReadFromServer(int addr, int nb, int ft, int bo, int dt);

	afx_msg int modbusAddMasterToRemoteSlaveRTU(BSTR device, int servId, int baud);
	afx_msg int modbusAddMasterToRemoteSlaveTCP(LPCTSTR ip, int port, int servID);
	afx_msg int modbusMastersUpdateConections();
	afx_msg int modbusRemoteMastersInitialization(int nb);

private:
	int initMastersForRemoteSlaves(const int nb);
	int modbusInteractionWithSlave(int addr, int nb, ModbusAdapterData* data, FunctionType ft, int index);
	int modbusInteractionWithSlaveLocal(int addr, int nb, ModbusAdapterData* data, FunctionType ft);
	int makeConnectionWithRemoteSlave(int index);

	//* FLOAT TO UINT & FLOAT FROM UINT *//
	uint16_t* FloatPtrToUint(const float* n, int size, ByteOrder order);
	float* UintToFloatPtr(const uint16_t* n, int size, ByteOrder order);

	//* 32 bit long TO UINT & 32 bit long  FROM UINT *//
	uint16_t* LongPtrToUint(const long* n, int size, ByteOrder order);
	long* UintToLongPtr(const uint16_t* n, int size, ByteOrder order);

	//* VARIANT STUFF *//
	int getPtrFromVARIANT( VARIANT  Data, ModbusAdapterData* &ptr, long &size);
	int getVARIANTFromPtr(VARIANT & resultVariant, ModbusAdapterData* &ptr, long size);

	ByteOrder getByteOrderFromInt(int bo);

// ����� �������
	DECLARE_EVENT_MAP()

// ���������� � �������� � �� �������
public:
	enum
	{
		dispidgetConnectionStatusInfo = 1L,
		dispidinitConnectionAsSlave = 2L,
		dispidstopSlave = 3L,
		dispidmodbusWriteToServer = 4L,
		dispidmodbusReadFromServer = 5L,
		dispidmodbusReadRegistersFromRemoteSlave = 6L,
		dispidmodbusWriteRegistersToRemoteSlave = 7L,
		dispidmodbusAddMasterToRemoteSlaveRTU = 8L,
		dispidmodbusAddMasterToRemoteSlaveTCP = 9L,
		dispidmodbusMastersUpdateConections = 10L,
		dispidmodbusRemoteMastersInitialization = 11L,		
	};

private:

	static  std::string connectionStatusInfo;
	static int salvePort; // ���� ���������� slave
	static std::string slaveIp; // ip ����� �������� slave
	static ModBusAdapter* localadapter; //��������� ������, �������������� ������������� � ��������� slave
	static int slaveThreadsCounter; // ������� ������� ��� ��������� slave (�� ������ ������ ���� slave � �������������� ���� �����)
	static bool slaveOnWork; // ����, ���������� ��������� ���������� slave

	ModBusAdapter** masters; // ������ ��������, ��� �������������� � ��������� Slaves
	MasterConnectionParams** mastersParams; // ������ ���������� ��������� ��� �������� slaves
	int mastersCounter; // ������� ��������, ���������� � ��������� slaves
	int modbusMastersForRemoteSlavesNumber;

protected:
	int slaveAddress;
};

