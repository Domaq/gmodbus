// GModbus.cpp: ���������� CGModbusApp � ����������� DLL.

#include "stdafx.h"
#include "GModbus.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CGModbusApp theApp;

const GUID CDECL _tlid = { 0xA6644559, 0xBFC9, 0x4EB8, { 0x80, 0x4, 0x4D, 0x4B, 0x10, 0x56, 0xE0, 0x4D } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CGModbusApp::InitInstance - ������������� DLL

BOOL CGModbusApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: �������� ����� ���� ��� ������������� ������.
		//CoInitializeEx(NULL, COINIT_MULTITHREADED);

	}

	return bInit;
}



// CGModbusApp::ExitInstance - ���������� DLL

int CGModbusApp::ExitInstance()
{
	// TODO: �������� ����� ���� ��� ���������� ������ ������.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - ��������� ������ � ��������� ������

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - ������� ������ �� ���������� �������

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
