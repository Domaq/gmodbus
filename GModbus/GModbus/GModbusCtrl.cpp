// GModbusCtrl.cpp: ���������� ������ CGModbusCtrl �������� ActiveX.

#include "stdafx.h"
#include "GModbus.h"
#include "GModbusCtrl.h"
#include "GModbusPropPage.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
//#include <vld.h>
#endif
 

static inline uint16_t bswap_16(uint16_t x)
{
	return (x >> 8) | (x << 8);
}

static inline uint32_t bswap_32(uint32_t x)
{
	return (bswap_16(x & 0xffff) << 16) | (bswap_16(x >> 16));
}


IMPLEMENT_DYNCREATE(CGModbusCtrl, COleControl)

// ����� ���������

BEGIN_MESSAGE_MAP(CGModbusCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()

// ����� ���������� � ��������

BEGIN_DISPATCH_MAP(CGModbusCtrl, COleControl)
	DISP_FUNCTION(CGModbusCtrl, "getConnectionStatusInfo", getConnectionStatusInfo, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CGModbusCtrl, "initConnectionAsSlave", initConnectionAsSlave, VT_I2, VTS_BSTR VTS_I2)
	DISP_FUNCTION(CGModbusCtrl, "stopSlave", stopSlave, VT_EMPTY, VTS_NONE)

	DISP_FUNCTION(CGModbusCtrl, "modbusWriteToServer", modbusWriteToServer, VT_I2, VTS_I2 VTS_I2 VTS_I2 VTS_I2 VTS_VARIANT)
	DISP_FUNCTION(CGModbusCtrl, "modbusReadFromServer", modbusReadFromServer, VT_VARIANT, VTS_I2 VTS_I2 VTS_I2 VTS_I2 VTS_I2)
	DISP_FUNCTION(CGModbusCtrl, "modbusReadRegistersFromRemoteSlave", modbusReadRegistersFromRemoteSlave, VT_I2, VTS_I2 VTS_I2 VTS_I2)
	DISP_FUNCTION(CGModbusCtrl, "modbusWriteRegistersToRemoteSlave", modbusWriteRegistersToRemoteSlave, VT_I2, VTS_I2 VTS_I2 VTS_I2)
	DISP_FUNCTION(CGModbusCtrl, "modbusAddMasterToRemoteSlaveRTU", modbusAddMasterToRemoteSlaveRTU, VT_I2, VTS_BSTR VTS_I2 VTS_I2 )
	DISP_FUNCTION(CGModbusCtrl, "modbusAddMasterToRemoteSlaveTCP", modbusAddMasterToRemoteSlaveTCP, VT_I2, VTS_BSTR VTS_I2 VTS_I2)
	DISP_FUNCTION(CGModbusCtrl, "modbusMastersUpdateConections", modbusMastersUpdateConections, VT_I2, VTS_NONE)
	DISP_FUNCTION(CGModbusCtrl, "modbusRemoteMastersInitialization", modbusRemoteMastersInitialization, VT_I2, VTS_I2)

	DISP_FUNCTION_ID(CGModbusCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()

// ����� �������

BEGIN_EVENT_MAP(CGModbusCtrl, COleControl)
END_EVENT_MAP()

// �������� �������

// TODO: ��� ������������� �������� �������������� �������� �������.  �� �������� ��������� �������� ��������.
BEGIN_PROPPAGEIDS(CGModbusCtrl, 1)
	PROPPAGEID(CGModbusPropPage::guid)
END_PROPPAGEIDS(CGModbusCtrl)

// ���������������� ������� ������ � guid

IMPLEMENT_OLECREATE_EX(CGModbusCtrl, "GMODBUS.GModbusCtrl.1",
	0xcf1216cc, 0x75bf, 0x4310, 0xa2, 0x92, 0xab, 0x91, 0x1d, 0x43, 0x9f, 0xb6)

	// ������� �� � ������ ����������

	IMPLEMENT_OLETYPELIB(CGModbusCtrl, _tlid, _wVerMajor, _wVerMinor)

	// �� ����������

	const IID IID_DGModbus = { 0x30330220, 0x7CA, 0x4CAE, { 0x96, 0x32, 0x37, 0x9, 0x98, 0xEF, 0xF6, 0x46 } };
const IID IID_DGModbusEvents = { 0x7AA52598, 0x6BF3, 0x4219, { 0x88, 0x1A, 0xF, 0xEF, 0x1C, 0xE4, 0x23, 0x1 } };

// �������� � ����� ��������� ����������

static const DWORD _dwGModbusOleMisc =
OLEMISC_ACTIVATEWHENVISIBLE |
OLEMISC_SETCLIENTSITEFIRST |
OLEMISC_INSIDEOUT |
OLEMISC_CANTLINKINSIDE |
OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CGModbusCtrl, IDS_GMODBUS, _dwGModbusOleMisc)

// CGModbusCtrl::CGModbusCtrlFactory::UpdateRegistry -
// ���������� ��� �������� ������� ���������� ������� ��� CGModbusCtrl

BOOL CGModbusCtrl::CGModbusCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: ���������, ��� �������� ���������� ������� �������� ������ ������������� �������.
	// �������������� �������� ��. � MFC TechNote 64.
	// ���� ������� ���������� �� ������������� �������� ������ ��������, ��
	// ���������� �������������� ����������� ���� ���, ������� �������� 6-�� ��������� �
	// afxRegApartmentThreading �� 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
		AfxGetInstanceHandle(),
		m_clsid,
		m_lpszProgID,
		IDS_GMODBUS,
		IDB_GMODBUS,
		afxRegApartmentThreading,
		_dwGModbusOleMisc,
		_tlid,
		_wVerMajor,
		_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}

modbus_mapping_t *ModBusAdapter::mb_mapping; // ����� ���������, � ������ �������� ��������� slave
int ModBusAdapter::server_socket; // ����� ��� TCP ���������� � ���������� Slave
modbus_t *ModBusAdapter::ctx; // modbuslib ��������, �������������� ���������� ������� MODBUS � ���������� modbuslib

int CGModbusCtrl::salvePort; // ���� ���������� slave
std::string CGModbusCtrl::slaveIp; // ip ����� �������� slave
ModBusAdapter* CGModbusCtrl::localadapter; //��������� ������, �������������� ������������� � ��������� slave

std::string CGModbusCtrl::connectionStatusInfo;
int CGModbusCtrl::slaveThreadsCounter; // ������� ������� ��� ��������� slave (�� ������ ������ ���� slave � �������������� ���� �����)
bool CGModbusCtrl::slaveOnWork; // ����, ���������� ��������� ���������� slave
//std::string CGModbusCtrl::connectionStatusInfo;

// CGModbusCtrl::CGModbusCtrl - �����������
CGModbusCtrl::CGModbusCtrl()
{
	InitializeIIDs(&IID_DGModbus, &IID_DGModbusEvents);
	// TODO: ��������������� ����� ������ ���������� �������� ����������.

	slaveThreadsCounter = 0;
	this->modbusMastersForRemoteSlavesNumber = 0;
	slaveOnWork = false;

	ModBusAdapter::mb_mapping = NULL;
	localadapter = NULL;

	this->masters = NULL;
	this->mastersParams = NULL;

	int rc = createMap(400);
	//  slaveAddress = 0;
	slaveAddress = 0;
}
// CGModbusCtrl::~CGModbusCtrl - ����������
CGModbusCtrl::~CGModbusCtrl()
{
	// TODO: ��������� ����� ������� ������ ���������� �������� ����������.
	this->stopSlave();
	modbus_mapping_free(ModBusAdapter::mb_mapping);
}

// CGModbusCtrl::OnDraw - ������� ���������

void CGModbusCtrl::OnDraw(
	CDC* pdc, const CRect& rcBounds, const CRect& /* rcInvalid */)
{
	if (!pdc)
		return;

	// TODO: �������� ��������� ��� ����������� ����� ���������.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}

// CGModbusCtrl::DoPropExchange - ��������� ����������

void CGModbusCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: �������� ������� PX_ ��� ������� ����������� �������������� ��������.
}


// CGModbusCtrl::OnResetState - ����� �������� ���������� � ��������� �� ���������

void CGModbusCtrl::OnResetState()
{
	COleControl::OnResetState();  // ���������� �������� �� ���������, ��������� � DoPropExchange

	// TODO: �������� ����� ��������� ������ ������� �������� ����������.
}


// CGModbusCtrl::AboutBox - ���������� ������������ ������ "� ���������"
void CGModbusCtrl::AboutBox()
{
	CDialogEx dlgAbout(IDD_ABOUTBOX_GMODBUS);
	dlgAbout.DoModal();
}


//* INFO *//
// ���������� ��������� ���������� � ������� ����������.
BSTR CGModbusCtrl::getConnectionStatusInfo()
{
	const char* result = this->connectionStatusInfo.c_str();
	CString resultstr = (CString)result;
	this->connectionStatusInfo.clear();
	return resultstr.AllocSysString();
}
// ������� ��������� �� ������
void CGModbusCtrl::showErrorMsgBox(std::string error)
{
	const char* result = error.c_str();
	CStringA message = (CStringA)result;
	MessageBox(message.AllocSysString(), _T("Error"), MB_ICONERROR | MB_OK);
}

//* MPDBUS SLAVE *//
int CGModbusCtrl::createMap(int start_addr)
{
	ModBusAdapter::server_socket = -1;
	ModBusAdapter::ctx = NULL;
	//ModBusAdapter::mb_mapping = modbus_mapping_new(MODBUS_MAX_READ_BITS, MODBUS_MAX_WRITE_BITS, MODBUS_MAX_READ_REGISTERS, MODBUS_MAX_WRITE_REGISTERS);
	ModBusAdapter::mb_mapping = modbus_mapping_new_start_address(start_addr, MODBUS_MAX_READ_BITS,
		start_addr, MODBUS_MAX_WRITE_BITS,
		start_addr, MODBUS_MAX_READ_REGISTERS,
		start_addr, MODBUS_MAX_WRITE_REGISTERS);
	if (ModBusAdapter::mb_mapping == NULL)
	{
		// ��������� ���������� � ������� �����
		this->connectionStatusInfo = "Failed to allocate the mapping \n";
		this->showErrorMsgBox(connectionStatusInfo);
		return _RC_MAP_ALLOCATION_ERROR_;
	}
	this->connectionStatusInfo = "Map allocated \n";
	return _RC_SUCCES_;
}
UINT CGModbusCtrl::initConnectionAsSlaveThreadFunction(LPVOID pParam)
{
	connectionStatusInfo += "Try to start Slave \n";
	slaveThreadsCounter++;
		
	// �������������� ��������� ����������
	MasterConnectionParams* mcplocal = new MasterConnectionParams(slaveIp, salvePort);
	// ������ �������, ������� ����� ��������� � ���� ���������� slave
	ModBusAdapter *slaveAdapter = new ModBusAdapter();
	// ��������� slave
	slaveAdapter->modbusSlaveStart(slaveOnWork, connectionStatusInfo, mcplocal);

	delete mcplocal;
	mcplocal = NULL;
	// ����� �������� slave ������ ������
	slaveThreadsCounter--;
	delete slaveAdapter;
	slaveAdapter = NULL;
	connectionStatusInfo += "Slave finished \n";
	// �������������� ����� ����������� ��� ������ �� ���� �������
	return 0;
}
// ������ Slave � �������� ����. ������������ NB_CONNECTION ���������
int CGModbusCtrl::initConnectionAsSlave(LPCTSTR ip, int port)
{
	// ���� ��� ������� slave, �� ��� ������ ��������� �����
	if (this->slaveOnWork)
	{
		connectionStatusInfo += "Slave has already run \n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		return _RC_SLAVA_ALREADY_EXIST_ERROR_;
	}
	// ��������, �� ��������� �� �� ����������������� ���������� �������
	int counter = slaveThreadsCounter;
	if (counter > MAX_THREADS)
	{
		connectionStatusInfo += "> MAX_THREADS for Slaves\n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		return _RC_MORE_THEN_MAX_THREADS_ERROR;
	}

	// ��������� ��������� ���������� slave
	CStringA tmp(ip);
	this->salvePort = port;
	this->slaveIp = tmp;

	// ��������� �����, ������ �������� ����� �������� slave
	AfxBeginThread(*initConnectionAsSlaveThreadFunction, NULL, THREAD_PRIORITY_NORMAL);
	int timeOutCounter = 0;
	do
	{
		//��� ���� Slave ������ ��������
		Sleep(500);
		timeOutCounter++;

		if (timeOutCounter == 10)
		{
			connectionStatusInfo += "Slave out of time\n";
			//this->showErrorMsgBox(this->connectionStatusInfo);
			return _RC_SLAVE_OUT_OF_TIME_ERROR_;
		}

	} while (slaveOnWork == false);

	// ������ ���������� ������� ��� ����� � ��������� Slave
	MasterConnectionParams* mcplocal = new MasterConnectionParams(slaveIp, salvePort);
	localadapter = new ModBusAdapter();

	// ������� �����������
	if (!localadapter->makeConnection(connectionStatusInfo, mcplocal))
	{
		delete mcplocal;
		delete localadapter;
		localadapter = NULL;

		connectionStatusInfo += "�� ����� ���������� ���������� ���������� ��������\n";
		this->showErrorMsgBox(this->connectionStatusInfo);
		return _RC_SLAVE_OUT_OF_TIME_ERROR_;
	}
	delete mcplocal;
	return _RC_SUCCES_;
}
// ������������� Slave
void CGModbusCtrl::stopSlave()
{
	//this->showErrorMsgBox("try to stop Slave");
	if (localadapter == NULL)
	{
		// ���� ���������� �������� �� ����������, ����� slave �� ��� ������� ���������
		return;
	}
	// ������������� ���� ����
	slaveOnWork = false;

	// ������ �������� ����������, ������� ��������� ������ slave � ������ ������, ��� ��� �� ���������� ���� slaveOnWork = false;
	MasterConnectionParams* mcplocal = new MasterConnectionParams(this->slaveIp, this->salvePort);
	ModBusAdapter *ad = new ModBusAdapter();
	ad->makeConnection(connectionStatusInfo, mcplocal);
	//this->showErrorMsgBox("stop flag state");

	// ������ ������� ��� ��������� ����������
	delete ad;
	ad = NULL;

	// ������ ��������� ������� 
	delete localadapter;
	localadapter = NULL;
	
	// ������ ��������� ����������
	delete mcplocal;
	mcplocal = NULL;

	// ������ ��������, ���������� � ��������� slaves
	if ((this->masters != NULL) && (this->mastersParams != NULL))
	{
		for (int i = 0; i < this->modbusMastersForRemoteSlavesNumber; i++)
		{
			//  ������ ��������
			if (this->masters[i] != NULL)
			{
				delete this->masters[i];
				this->masters[i] = NULL;
			}

			// ������ ���������
			if (this->mastersParams[i] != NULL)
			{
				delete this->mastersParams[i];
				this->mastersParams[i] = NULL;
			}
		}
		//  ������ ��������� �� ��������
		delete[] this->masters;
		this->masters = NULL;

		// ������ ��������� �� ���������
		delete[] this->mastersParams;
		this->mastersParams = NULL;
	}
}

//* MODBUS MASTER *//
int CGModbusCtrl::modbusReadRegistersFromRemoteSlave(int addr, int nb, int rmtSlaveIndex)
{
	FunctionType ft_remote = FunctionType::Read_Holding_Registers;
	FunctionType ft_local = FunctionType::Write_Multiple_Registers;

	ModbusAdapterData* mdData = new ModbusAdapterData(nb, DataType::REGISTRS);

	// REMOTE START
	int rc = modbusInteractionWithSlave(addr, nb, mdData, ft_remote, rmtSlaveIndex);
	// REMOTE END

	if (rc != _RC_SUCCES_)
	{
		delete mdData;
		return rc;
	}

	// LOCAL START
	rc = modbusInteractionWithSlaveLocal(addr, nb, mdData, ft_local);
	// LOCAL END

	delete mdData;
	return rc;
}
int CGModbusCtrl::modbusWriteRegistersToRemoteSlave(int addr, int nb, int rmtSlaveIndex)
{
	FunctionType ft_local = FunctionType::Read_Holding_Registers;
	FunctionType ft_remote = FunctionType::Write_Multiple_Registers;

	ModbusAdapterData* mdData = new ModbusAdapterData(nb, DataType::REGISTRS);

	// LOCAL START
	int rc = modbusInteractionWithSlaveLocal(addr, nb, mdData, ft_local);
	// LOCAL END


	if (rc != _RC_SUCCES_)
	{
		delete mdData;
		return rc;
	}

	// REMOTE START
	rc = modbusInteractionWithSlave(addr, nb, mdData, ft_remote, rmtSlaveIndex);
	// REMOTE END

	delete mdData;
	return rc;
}

// ���������� �� SLAVE ����� ��������, ���������� �  VARIANT (FLOAT_PTR ��� LONG_PTR)
int CGModbusCtrl::modbusWriteToServer(int addr, int nb, int ft, int bo, const VARIANT &Data)
{
	// ��������� ��� ����� �������� ���������� ������� �� Modbuslib
	if (ft != FunctionType::Write_Multiple_Registers)
	{
		AfxMessageBox(_T("ERROR! \n modbusWriteRegistersToServer: Wrong function type! "));
		this->connectionStatusInfo += "ERROR! \n modbusWriteRegistersToServer: Wrong arguments! \n";
		return _RC_INCORRECT_FUNCTION_TYPE_ERROR_;
	}

	long len = 0;
	ModbusAdapterData* input = NULL;
	int rc = this->getPtrFromVARIANT(Data, input, len);
	if (rc != _RC_SUCCES_)
	{
		AfxMessageBox(_T("ERROR! \n modbusWriteRegistersToServer: Wrong arguments Data! "));
		this->connectionStatusInfo += "ERROR! \n modbusWriteRegistersToServer: Wrong arguments Data! \n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		delete input;
		return rc;
	}

	// �������� ������ ������! ����� ����� �������� � ��������� ������������ ��������� �++
	/*CString msgstr;
	long * test = input->getLongs();
	msgstr.Format(_T("long = %d"),test[1]);
	AfxMessageBox(msgstr);//*/
	//��������� ��� ��������� (==2)
	if (len * 2 != nb)
	{
		//AfxMessageBox(_T("!!"));
		this->connectionStatusInfo += "ERROR! \n modbusWriteRegistersToServer: Wrong arguments! \n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		delete input;
		return _RC_INCORRECT_INPUT_DATA_ERROR_;
	}
	// ���������, ��� ���������� �����-�� ������� ���������� ����
	ByteOrder order = getByteOrderFromInt(bo);
	if (order == ByteOrder::DEFAULT)
	{
		this->connectionStatusInfo += "ERROR! \n modbusWriteRegistersToServer: Wrong arguments! \n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		delete input;
		return _RC_INCORRECT_INPUT_DATA_ERROR_;
	}
	// ������������ � ����, ����������� modbuslib
	ModbusAdapterData* mdData = NULL;
	switch (input->getDataType())
	{
		case(DataType::FLOATS) :
		{
			//AfxMessageBox(_T("FLOAT "));
			mdData = new ModbusAdapterData(FloatPtrToUint(input->getFloats(), len, order));
			break;
		}
		case(DataType::LONGS) :
		{
			mdData = new ModbusAdapterData(LongPtrToUint(input->getLongs(), len, order));
			break;
		}
		default:
		{
			// �� � ����� ����������� ��� ������� ������, ���������� ������
			this->connectionStatusInfo += "ERROR! \n modbusWriteRegistersToServer: Wrong arguments type! \n";
			//this->showErrorMsgBox(this->connectionStatusInfo);
			delete input;
			return _RC_INCORRECT_INPUT_DATA_ERROR_;
		}
	}
	delete input;

	// ������� ������ ������ � �����
	rc = modbusInteractionWithSlaveLocal(addr, nb, mdData, (FunctionType)ft);
	delete mdData;
	return rc;
}
// ������ �� SLAVE ����� nb/2 32-bit �������� �� ������ addr � ���������� �� � VARIANT  
VARIANT CGModbusCtrl::modbusReadFromServer(int addr, int nb, int ft, int bo, int dt)
{
	VARIANT resultVariant;
	resultVariant.vt = VT_EMPTY;

	if ((nb % 2) != 0)
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusReadRegistersFromServer: Wrong arguments! \n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		return resultVariant;
	}

	if (ft != FunctionType::Read_Holding_Registers && ft != FunctionType::Read_Input_Registers)
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusReadRegistersFromServer: Wrong arguments! \n";
		//this->showErrorMsgBox(this->connectionStatusInfo);
		return resultVariant;
	}

	// ��� ����� ����� ��������������, ������� ������ ������ � �����
	ModbusAdapterData* mdData = new ModbusAdapterData(nb, DataType::REGISTRS);
	int rc = modbusInteractionWithSlaveLocal(addr, nb, mdData, (FunctionType)ft);

	if (rc != _RC_SUCCES_)
	{
		return resultVariant;
	}

	ByteOrder order = getByteOrderFromInt(bo);
	if (order == ByteOrder::DEFAULT)
	{
		// ���� �� �������, ����� ������������ ������� ���������� ����, �� ������ � �� ������
		this->connectionStatusInfo += "CGModbusCtrl::modbusReadRegistersFromServer: Wrong ByteOrder! \n";
		return resultVariant;
	}

	ModbusAdapterData* output = NULL;
	switch (dt)
	{
		case(DataType::FLOATS) :
		{
			output = new ModbusAdapterData(this->UintToFloatPtr(mdData->getRegisters(), nb / 2, order));
			break;
		}
		case(DataType::LONGS) :
		{
			output = new ModbusAdapterData(this->UintToLongPtr(mdData->getRegisters(), nb / 2, order));
			break;
		}
		default:
		{
			// ���� �� �������, ����� ���������� ���, �� ������ � �� ������
			this->connectionStatusInfo += "CGModbusCtrl::modbusReadRegistersFromServer: Wrong Type! \n";
			return resultVariant;
		}
	}

	delete mdData;

	// ������ ������ VARIANT ��� ��������
	rc = this->getVARIANTFromPtr(resultVariant, output, nb / 2);
	if (rc != _RC_SUCCES_)
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusReadRegistersFromServer: Error Variant convertation \n";
	}
	delete output;
	return resultVariant;
}

int CGModbusCtrl::modbusAddMasterToRemoteSlaveRTU(BSTR device, int servId, int baud)
{
	if(this->mastersCounter >= this->modbusMastersForRemoteSlavesNumber)
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusAddMasterToRemoteSlaveRTU: Remote masters overflow \n";
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	if ((this->masters == NULL) || (this->mastersParams == NULL))
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusAddMasterToRemoteSlaveRTU: Remote masters are NULL\n";
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	this->mastersParams[this->mastersCounter] = new MasterConnectionParams(device, servId, baud, 'N');
	return makeConnectionWithRemoteSlave(this->mastersCounter);
}
int CGModbusCtrl::modbusAddMasterToRemoteSlaveTCP(LPCTSTR ip, int port, int servID)
{	
	//AfxMessageBox(_T("modbusAddMasterToRemoteSlaveTCP - 1"));
	if (this->mastersCounter >= this->modbusMastersForRemoteSlavesNumber)
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusAddMasterToRemoteSlaveTCP: Remote masters overflow \n";
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	//AfxMessageBox(_T("modbusAddMasterToRemoteSlaveTCP - 2"));
	if ((this->masters == NULL) || (this->mastersParams == NULL))
	{
		this->connectionStatusInfo += "CGModbusCtrl::modbusAddMasterToRemoteSlaveTCP: Remote masters are NULL\n";
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	/*CString msgstr;
	msgstr.Format(_T("slave 1 id = %d"), servID);
	AfxMessageBox(msgstr);//*/
	this->mastersParams[this->mastersCounter] = new MasterConnectionParams(ip , port, servID);
	//AfxMessageBox(_T("modbusAddMasterToRemoteSlaveTCP - 3"));
	return makeConnectionWithRemoteSlave(this->mastersCounter);
}

int CGModbusCtrl::modbusMastersUpdateConections()
{
	for (int i = 0; i < this->modbusMastersForRemoteSlavesNumber; i++)
	{
		if (this->masters[i] == NULL)
		{
			int rc = makeConnectionWithRemoteSlave(i);
			if (rc != _RC_SUCCES_)
			{
				return rc;
			}
		}
	}
	return _RC_SUCCES_;
}

int CGModbusCtrl::modbusRemoteMastersInitialization(int nb)
{
	if (this->modbusMastersForRemoteSlavesNumber != 0)
	{
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	this->modbusMastersForRemoteSlavesNumber = nb;
	return this->initMastersForRemoteSlaves(nb);
}

/// PRIVATE STUFF ///
int CGModbusCtrl::initMastersForRemoteSlaves(const int nb)
{
	if (nb <= 0)
	{
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	this->masters = new ModBusAdapter*[nb];
	this->mastersParams = new MasterConnectionParams*[nb];
	for (size_t i = 0; i < (size_t)nb; ++i)
	{
		this->masters[i] = NULL;
		this->mastersParams[i] = NULL;
	}
	this->mastersCounter = 0;
	return _RC_SUCCES_;
}
int CGModbusCtrl::makeConnectionWithRemoteSlave(int index)
{
	if ((index >= this->modbusMastersForRemoteSlavesNumber) || (this->mastersParams[index] == NULL))
	{
		this->connectionStatusInfo += "CGModbusCtrl::makeConnectionWithRemoteSlave: error with master params\n";
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}
	this->masters[index] = new ModBusAdapter();
	//AfxMessageBox(_T("makeConnectionWithRemoteSlave"));
	int rc = this->masters[index]->makeConnection(connectionStatusInfo, this->mastersParams[index]);
	if (rc != _RC_SUCCES_)
	{
		connectionStatusInfo += modbus_strerror(errno);
		delete this->masters[index];
		this->masters[index] = NULL;

		//delete this->mastersParams[index];
		//this->mastersParams[index] = NULL;

		return rc;
	}
	this->mastersCounter++;
	return rc;
}
int CGModbusCtrl::modbusInteractionWithSlave(int addr, int nb, ModbusAdapterData* data, FunctionType ft, int index)
{
	if ((index >= this->modbusMastersForRemoteSlavesNumber) || (this->masters[index]->getconstatus() == false))
	{
		return _RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_;
	}

	int rc = this->masters[index]->modbusInteractionWithSlave(addr, nb, data, ft, connectionStatusInfo);
	if (rc < 0)
	{
		// ��� ����� ���������� ������������ ����������
		delete this->masters[index];
		this->masters[index] = NULL;
		connectionStatusInfo += "Bad connection on index: "   + std::to_string(index) +  " \n";
	}
	return rc;
}
int CGModbusCtrl::modbusInteractionWithSlaveLocal(int addr, int nb, ModbusAdapterData* data, FunctionType ft)
{
	return localadapter->modbusInteractionWithSlave(addr, nb, data, ft, connectionStatusInfo);
}
//* FLOAT TO UINT & FLOAT FROM UINT *//
/// ������������ ������ float-�� ������� size � ������ �� 2 � size uint16_t
uint16_t* CGModbusCtrl::FloatPtrToUint(const float* n, int size, ByteOrder order)
{
	uint16_t* result = new uint16_t[size * 2];
	for (int i = 0; i < size; ++i)
	{
		//uint32_t tmp = (uint32_t)(*(uint32_t*)&n[i]);
		//result[i * 2] = (uint16_t)(tmp >> 16);
		//result[i * 2 + 1] = (uint16_t)(tmp & 0x0000FFFFuL);

		uint16_t* tmp = new uint16_t[2];

		switch (order)
		{
		case ABCD:
		{
			modbus_set_float_abcd(n[i], tmp);
			break;
		}
		case DCBA:
		{
			modbus_set_float_dcba(n[i], tmp);
			break;
		}
		case BADC:
		{
			modbus_set_float_badc(n[i], tmp);
			break;
		}
		case CDAB:
		{
			modbus_set_float_cdab(n[i], tmp);
			break;
		}
		default:
			modbus_set_float(n[i], tmp);
			break;
		}

		result[i * 2] = tmp[0];
		result[i * 2 + 1] = tmp[1];

		delete[] tmp;
	}
	return result;
}
/// ������������ ������ �� 2 � size uint16_t � ������ float-�� ������� size 
float* CGModbusCtrl::UintToFloatPtr(const uint16_t* n, int size, ByteOrder order)
{
	float *result = new float[size];

	for (int i = 0; i < size; ++i)
	{
		uint16_t* tmp = new uint16_t[2];
		tmp[0] = n[i * 2];
		tmp[1] = n[i * 2 + 1];
		switch (order)
		{
		case ABCD:
		{
			result[i] = modbus_get_float_abcd(tmp);
			break;
		}
		case DCBA:
		{
			result[i] = modbus_get_float_dcba(tmp);
			break;
		}
		case BADC:
		{
			result[i] = modbus_get_float_badc(tmp);
			break;
		}
		case CDAB:
		{
			result[i] = modbus_get_float_cdab(tmp);
			break;
		}
		default:
			result[i] = modbus_get_float(tmp);
			break;
		}

		delete[] tmp;

		//uint32_t tmp = (((uint32_t)n[i * 2]) << 16) | ((uint32_t)n[i * 2 + 1]);
		//result[i] = (float)(*(float*)&tmp);
	}
	return result;
}

//* 32 bit long TO UINT & 32 bit long  FROM UINT *//
/// ������������ ������ long-�� ������� size � ������ �� 2 � size uint16_t
uint16_t* CGModbusCtrl::LongPtrToUint(const long* n, int size, ByteOrder order)
{
	uint16_t* result = new uint16_t[size * 2];
	int32_t lval;

	for (int i = 0; i < size; ++i)
	{
		memcpy(&lval, &n[i], sizeof(int32_t));

		switch (order)
		{
		case ABCD:
		{
			lval = htonl(lval);
			result[i * 2] = (uint16_t)(lval >> 16);
			result[i * 2 + 1] = (uint16_t)lval;
			break;
		}
		case DCBA:
		{
			lval = bswap_32(htonl(lval));
			result[i * 2] = (uint16_t)(lval >> 16);
			result[i * 2 + 1] = (uint16_t)lval;
			break;
		}
		case BADC:
		{
			lval = htonl(lval);
			result[i * 2] = (uint16_t)bswap_16(lval >> 16);
			result[i * 2 + 1] = (uint16_t)bswap_16(lval & 0xFFFF);
			break;
		}
		case CDAB:
		{
			lval = htonl(lval);
			result[i * 2] = (uint16_t)lval;
			result[i * 2 + 1] = (uint16_t)(lval >> 16);
			break;
		}
		default:
			break;
		}
	}
	return result;

}
/// ������������ ������ �� 2 � size uint16_t � ������ long-�� ������� size 
long* CGModbusCtrl::UintToLongPtr(const uint16_t* n, int size, ByteOrder order)
{
	long *result = new long[size];

	for (int i = 0; i < size; ++i)
	{
		//uint16_t* tmp = new uint16_t[2];
		//tmp[0] = n[i * 2];
		//tmp[1] = n[i * 2 + 1];
		switch (order)
		{
		case ABCD:
		{
			long f;
			int32_t lval;

			lval = ntohl(((int32_t)n[i * 2] << 16) + n[i * 2 + 1]);
			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_abcd(tmp);
			break;
		}
		case DCBA:
		{
			long f;
			int32_t lval;

			lval = ntohl(bswap_32((((uint32_t)n[i * 2]) << 16) + n[i * 2 + 1]));

			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_dcba(tmp);
			break;
		}
		case BADC:
		{
			long f;
			int32_t lval;

			lval = ntohl((uint32_t)(bswap_16(n[i * 2]) << 16) + bswap_16(n[i * 2 + 1]));

			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_badc(tmp);
			break;
		}
		case CDAB:
		{
			long f;
			int32_t lval;

			lval = ntohl((((uint32_t)n[i * 2 + 1]) << 16) + n[i * 2]);


			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_cdab(tmp);
			break;
		}
		default:
			//result[i] = modbus_get_float(tmp);
			break;
		}

		//delete[] tmp;

	}
	return result;
}

//* VARIANT STUFF *//
int CGModbusCtrl::getPtrFromVARIANT( VARIANT  Data, ModbusAdapterData* &ptr, long &size)
{
	// ��������� ��� �������� ������ ������
	if ((Data.vt & VT_ARRAY) == VT_ARRAY)
	{
		// ������ ������
		SAFEARRAY *pSA;
		pSA = Data.parray;

		// ��������� �������
		LONG uBound;
		SafeArrayGetUBound(pSA, 1, &uBound);

		LONG lBound;
		SafeArrayGetLBound(pSA, 1, &lBound);

		//��������� �����
		long len = uBound - lBound + 1;

		
		int type = Data.vt & (VT_ARRAY - 1);
		// ��������� ��� �������� ������ float!!
		if (type == VT_R4)
		{
			//AfxMessageBox(_T("!! "));
			float *dest = new float[len];

			// �������� ������
			float value = 0;
			for (long j = lBound; j < len; ++j)
			{
				SafeArrayGetElement(pSA, &j, &value);
				*(dest + j - lBound) = value;
			}

			size = len;
			ptr = new ModbusAdapterData(dest);
			return _RC_SUCCES_;

		}
		else if (type == VT_I4)
		{
			//AfxMessageBox(_T("! "));

			long *dest = new long[len];

			// �������� ������
			long value = 0;
			for (long j = lBound; j < len; ++j)
			{
				SafeArrayGetElement(pSA, &j, &value);
				*(dest + j - lBound) = value;
			}

			size = len;
			ptr = new ModbusAdapterData(dest);
			return _RC_SUCCES_;
		}
		else
		{
			size = 0;
			ptr = nullptr;
			return _RC_INCORRECT_INPUT_DATA_ERROR_;
		}
	}
	else
	{
		size = 0;
		ptr = nullptr;
		return _RC_INCORRECT_INPUT_DATA_ERROR_;
	}
	return _RC_UNKNOWN_ERROR_;
}
int CGModbusCtrl::getVARIANTFromPtr(VARIANT & resultVariant, ModbusAdapterData* &ptr, long size)
{

	// ����� ������� �������
	SAFEARRAYBOUND rgsabound[1];
	rgsabound[0].lLbound = 0;
	rgsabound[0].cElements = size;
	SAFEARRAY *pSA2 = NULL;

	if (ptr->getDataType() == DataType::FLOATS)
	{
		//������ ���������� ������ float-�� ������� ������
		pSA2 = SafeArrayCreate(VT_R4, 1, rgsabound);
		resultVariant.vt = VT_R4 | VT_ARRAY;
		float value = 0;

		// ��������� ���
		float* fl = ptr->getFloats();
		for (long j = 0; j < size; j++)
		{
			value = fl[j];
			SafeArrayPutElement(pSA2, &j, &value);
		}

	}

	if (ptr->getDataType() == DataType::LONGS)
	{
		//������ ���������� ������ long-�� ������� ������
		pSA2 = SafeArrayCreate(VT_I4, 1, rgsabound);
		resultVariant.vt = VT_I4 | VT_ARRAY;
		long value = 0;

		// ��������� ���
		long* fl = ptr->getLongs();
		for (long j = 0; j < size; j++)
		{
			value = fl[j];
			SafeArrayPutElement(pSA2, &j, &value);
			//this->showErrorMsgBox(std::to_string(value));
		}

	}

	resultVariant.parray = pSA2;
	return 1;
}

ByteOrder CGModbusCtrl::getByteOrderFromInt(int bo)
{
	switch (bo)
	{
	case(0) :
	{
		return ByteOrder::ABCD;
	}
	case(1) :
	{
		return ByteOrder::DCBA;
	}
	case(2) :
	{
		return ByteOrder::BADC;
	}
	case(3) :
	{
		return ByteOrder::CDAB;
	}
	default:
		return ByteOrder::DEFAULT;
	}
}
