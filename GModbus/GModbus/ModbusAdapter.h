#pragma once

#include "../libmodbus/headers/modbus.h"
#pragma comment(lib, "../libmodbus/modbus.lib")

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <afxmt.h>

#if defined(_WIN32)
#pragma comment(lib, "WS2_32.lib")
#include <ws2tcpip.h>
#else
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include <string>

#define NB_CONNECTION 9
#define MAX_THREADS 1

//#define ONE_THREAD_MODE
#define TWO_THREAD_MODE

typedef enum {
	_RC_UNKNOWN_ERROR_ = -1,
	_RC_SLAVA_ALREADY_EXIST_ERROR_ = -2,
	_RC_MORE_THEN_MAX_THREADS_ERROR = -3,
	_RC_SLAVE_OUT_OF_TIME_ERROR_ = -4,
	_RC_MAP_ALLOCATION_ERROR_ = -5,
	_RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_ = -6,
	_RC_INCORRECT_FUNCTION_TYPE_ERROR_ = -7,
	_RC_INCORRECT_INPUT_DATA_ERROR_ = -8,
	_RC_NOT_NULL_ERROR_ = -9,
	_RC_CONNECTION_ERROR = -10,
	_RC_SUCCES_ = 1
} ResultStatuses;

/*!
\brief
������������ ConnectionType ���������� ���������� ���� �����������, ������� ����� ����������� ModbusMaster
**/
typedef enum{
	TCP,
	RTU,
	NOTYPE
} ConnectionType;
/*!
\brief
������������ DataType ���������� ���������� ���� ������, ������� ����� ���� �������� ��� ������������� ModbusMaster
**/
typedef enum{
	BITS,
	REGISTRS, // 32-bit values
	FLOATS, // 32-bit values
	LONGS, // 32-bit values
	NODATATYPE
} DataType;
/*!
\brief
������������ ByteOrder ���������� ���������� ������� ���������� ���� � 32-������ ��������� , ������� ����� ���� �������� ��� ������������� ModbusMaster
**/
typedef enum {
	ABCD = 0,
	DCBA = 1,
	BADC = 2,
	CDAB = 3,
	DEFAULT = -1
} ByteOrder;
/*!
\brief
������������ FunctionType ���������� ����������� ����������� ������� ��������� Modbus
**/
typedef enum {
	Read_Coil_Status = 1,
	Read_Discrete_Inputs = 2,
	Read_Holding_Registers = 3,
	Read_Input_Registers = 4,
	Write_Multiple_Coils = 15,
	Write_Multiple_Registers = 16
} FunctionType;
/*!
\brief
����� MasterConnectionParams ������������ ������ � ����������� �����������  ��� ������������� ModbusMaster
**/
class MasterConnectionParams
{
public:

	MasterConnectionParams() : tp(ConnectionType::NOTYPE), ip(""), port(-1), device(""), slaveId(-1), baud(-1), parity('c'), data_bit(-1), stop_bit(-1) {};
	MasterConnectionParams(LPCTSTR ip, int port, int slaveID) : device(""), baud(-1), parity('c'), data_bit(-1), stop_bit(-1)
	{
		//AfxMessageBox(_T("MasterConnectionParams1"));
		CStringA tmp(ip);
		std::string resultip = tmp;
		this->tp = ConnectionType::TCP;
		this->ip = resultip;
		this->port = port;
		this->slaveId = slaveID;
	};

	MasterConnectionParams(std::string ip, int port) : device(""), slaveId(1), baud(-1), parity('c'), data_bit(-1), stop_bit(-1)
	{
		//AfxMessageBox(_T("MasterConnectionParams 3"));
		this->tp = ConnectionType::TCP;
		this->ip = ip;
		this->port = port;
	};//*/

	MasterConnectionParams(LPCTSTR device, int slaveId, int baud, char cparity) : ip(""), port(-1), data_bit(8), stop_bit(1)
	{
		//AfxMessageBox(_T("MasterConnectionParams 4"));
		CStringA tmp(device);
		std::string resultDevice = tmp;
		this->tp = ConnectionType::RTU;
		this->device = resultDevice;
		this->slaveId = slaveId;
		this->baud = baud;
		this->parity = cparity;
	};//
	~MasterConnectionParams() {};

	ConnectionType tp;

	//tcp
	std::string ip;
	int port;

	//rtu
	std::string device; // ����� ����������
	int slaveId; // ����� ��������� slave
	int baud; // ��������
	char parity;
	int data_bit;
	int stop_bit;

};
/*!
\brief
ModbusAdapterData ������������ ������ � ���� �����, ������������� ������������� DataType
**/
class ModbusAdapterData
{
public:
	ModbusAdapterData();
	ModbusAdapterData(uint16_t* registers);
	ModbusAdapterData(uint8_t* bits);
	ModbusAdapterData(float* flvals);
	ModbusAdapterData(long* lvals);
	ModbusAdapterData(int nb, DataType dp);

	~ModbusAdapterData();

	DataType getDataType(){ return this->tp; };
	uint16_t* getRegisters() { return this->registers; };
	uint8_t* getBits() { return this->bits; };
	float* getFloats() { return this->flvals; };
	long* getLongs() { return this->lvals; };

	bool setRegisters(uint16_t* registers);
	bool setBits(uint8_t* bits);
	bool setFloats(float* flvals);
	bool setLongs(long* lvals);


private:

	DataType tp;

	uint16_t* registers;
	uint8_t*  bits;

	float* flvals;
	long* lvals;
};
/*!
\brief
ModBusAdapter ������������ ������ � ����������� Modbuslib
**/
class ModBusAdapter
{
public:
	ModBusAdapter();
	virtual ~ModBusAdapter();
	bool getconstatus() { return this->masterConnectionStatus; };
	void setconstatus(bool stat) { this->masterConnectionStatus = stat; };

	//* MPDBUS ADAPTER SLAVE *//
	static void close_sigint(int dummy)
	{
		if (server_socket != -1)
		{
			closesocket(server_socket);
		}
		modbus_free(ctx);
		ctx = NULL;
	}
	/*!  modbusSlaveStart(bool & stopflag, std::string &err);
	\brief
	��������� ModBusAdapter-Slave
	\param stopflag ���� ��������� ModBusAdapter-Slave
	\param err ���������, ��� �������� ������ ������ ModBusAdapter-Slave
	*/
	void modbusSlaveStart(bool & stopflag, std::string &err, MasterConnectionParams* &mcp);

	static int server_socket;
	static modbus_mapping_t *mb_mapping; // ����� ���������, �������� � ������ ������ ����������, � ����������� ����� ��������� ������. 
										 //! �� ������� �� ������� ����������� ModBusAdapter
	static modbus_t *ctx; // bodbuslib-ctx ��� ������ slave

	//* MPDBUS ADAPTER MASTER *//	

	/*!  BOOL makeConnection(std::string &err, ConnectionType ct);
	\brief
	������������� ���������� � ��������� Slave
	\param err  ���������, ��� �������� ������ ������ ModBusAdapter-Master 
	\param ct ��� ����������, ������� ������� Slave
	*/
	int makeConnection(std::string &err, MasterConnectionParams* &mcp);
	/*!  int modbusInteractionWithSlave(int addr, int nb, ModbusAdapterData* data, FunctionType ft, std::string &err);
	\brief   ������������ �������������� � �������� modbuslib, ���������������� ��� ������ (������/�������) � ��������� slave

	\details �� �������� ft ��������� ��������������� ������� modbuslib, ������� ������������ ������ ��� ������ ������ data ��� ������ �� slave,
	\details � �������� ��������� ������ ��������� ModbusAdapter-Master.

	\param addr  ����� ������ ������ � ����� ���������, � �������� ����� ����������� ��������������
	\param nb  ���������� ������(����/��������) � �����, � �������� ����� ����������� ��������������
	\param data ��������� �� ������
	\param ft ��� ������� ��������� modbus
	\param err  ���������, ��� �������� ������ ������ ModBusAdapter-Master
	*/
	int modbusInteractionWithSlave(int addr, int nb, ModbusAdapterData* data, FunctionType ft, std::string &err);

private:
	/*!  int performWithRegistersFunctions(int addr, int nb, uint16_t* result, FunctionType ft, std::string &err);
	\brief   ������������ �������������� � �������� modbuslib, ���������������� ��� ������ � ���������� (������/�������)

	\details �� �������� ft ��������� ��������������� ������� modbuslib, ������� ������������ ������ ��� ������ ��������� ��� ������ �� slave,
	\details � �������� ��������� ������ ��������� ModbusAdapter-Master.

	\param addr  ����� ������� �������� � ����� ���������
	\param nb  ���������� ��������� 
	\param result ��������� �� ������
	\param ft ��� ������� ��������� modbus
	\param err  ���������, ��� �������� ������ ������ ModBusAdapter-Master
	*/
	int performWithRegistersFunctions(int addr, int nb, uint16_t* result, FunctionType ft, std::string &err);
	/*!  int performWithBitsFunctions(int addr, int nb, uint8_t* result, FunctionType ft, std::string &err);
	\brief   ���������� performWithRegistersFunctions, ������ ��� ������ � ������
	*/
	int performWithBitsFunctions(int addr, int nb, uint8_t* result, FunctionType ft, std::string &err);

	modbus_t *master_ctx;
	bool masterConnectionStatus;
};

