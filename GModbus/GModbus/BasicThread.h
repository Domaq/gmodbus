#if _MSC_VER > 1000
#pragma once
#endif

#include "stdafx.h"
#include "atlbase.h"

#ifndef __basicthread_h
#define __basicthread_h
#include "string"
#include <process.h>



class CBasicThread
{
	//typedef ThreadModel _ThreadModel;


	//typedef typename _ThreadModel::AutoCriticalSection _CritSec;
	
	
	//typedef CComMultiThreadModel ThreadModel; 
	//typedef ThreadModel _ThreadModel;
	//typedef _ThreadModel::AutoCriticalSection _CritSec;
	//*/
	
	/*

public: 
	static unsigned int __stdcall RunProc( void *pvoid );
protected:
	static 	_CritSec m_sharedCrSect;

public:
	CBasicThread();
	~CBasicThread();
	void StartThread(int nPriority = THREAD_PRIORITY_NORMAL); 
					    // starts the thread, call TerminateThread to stop the thread
						// StartThread calls Run() that iterates until TerminateThread 
						// is called
						// Use the virtual method Command to specify what you want to 
						// do during the iterations
	void TerminateThread();
protected:
//	const HWND & m_hWnd; // handle for main window. Used for safePostMessage
	int Run();
	virtual bool MayIContinue();
	std::string m_threadname;
//	bool safePostMessage( UINT message, WPARAM wParam, LPARAM lParam);
	HANDLE m_workerThread;
private:
	bool m_continue;
	bool GetContinue();
	void SetContinue(bool val);
	_CritSec m_CritSec;

public:
	virtual void PreCommand()=0;
	virtual void PostCommand(){};
	virtual void Command() = 0; // the method to further bind. All parameters should 
								// be set using a new method or 
			//*/
};

#endif