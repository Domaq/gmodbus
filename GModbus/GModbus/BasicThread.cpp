#include "stdafx.h"
#include "BasicThread.h"


//CBasicThread::_CritSec CBasicThread::m_sharedCrSect;

/*
bool CBasicThread::safePostMessage( UINT message, WPARAM wParam, LPARAM lParam)
{
	bool bResult = true;
	m_sharedCrSect.Lock();
	if (IsWindow(m_hWnd))
	{
		if (!::PostMessage(m_hWnd, message, wParam, lParam))
		{
			MessageBox(NULL, "Could not Post Message!", "ATL Ctrl Error", MB_OK);
		}
	}
	else
	{
		::MessageBox(NULL, "Not a HWND", "ATL Ctrl Error", MB_OK);
		bResult = false;
	}
	m_sharedCrSect.Unlock();
	return bResult;
}
//*/

/*

CBasicThread::CBasicThread()
{
	SetContinue(true);
	m_workerThread = NULL;
}

CBasicThread::~CBasicThread()
{
}

// static method
unsigned int __stdcall CBasicThread::RunProc( void *pvoid )
{
	CBasicThread *pThread = static_cast<CBasicThread *>(pvoid);
	return pThread->Run();
}


void CBasicThread::StartThread(int nPriority)
{
	unsigned unThreadAddr;
	m_workerThread = reinterpret_cast<HANDLE>
		(_beginthreadex(NULL, 0, RunProc, this, 1, &unThreadAddr));
}

bool CBasicThread::MayIContinue()
{
	bool result = GetContinue();
	return result;
}

int CBasicThread::Run()
{
	PreCommand();
	while (MayIContinue())
	{
		Command();
	}
	PostCommand();
	return 0;
}

void CBasicThread::TerminateThread()
{
	SetContinue(false); // Terminates the thread by asking it to do it
	::WaitForSingleObject( m_workerThread, INFINITE ); // Waits until the thread is terminated
}

bool CBasicThread::GetContinue()
{
	bool returnVal;
	m_CritSec.Lock();
	returnVal = m_continue;
	m_CritSec.Unlock();
	return returnVal;
}

void CBasicThread::SetContinue(bool val)
{
	m_CritSec.Lock();
	m_continue = val;
	m_CritSec.Unlock();
}
//*/

