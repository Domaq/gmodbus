// libbodbustest.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
//#include <vld.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "libmodbus/headers/modbus.h"
#pragma comment(lib, "libmodbus/modbus.lib")

#define ADDRESS_START    0
#define ADDRESS_END     99
#define SERVER_ID       17

#define FLOAT_ARRAY_DIMENSION    6

#if defined(_WIN32)
#pragma comment(lib, "WS2_32.lib")
#include <ws2tcpip.h>
#else
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif


typedef enum {
	TCP,
	RTU
} Type;

typedef enum {
	ABCD,
	DCBA,
	BADC,
	CDAB,
} ByteOrder;

typedef struct
{
	std::string ip;
	int port;
} ParamS;

uint16_t bswap_16(uint16_t x)
{
	return (x >> 8) | (x << 8);
}

uint32_t bswap_32(uint32_t x)
{
	return (bswap_16(x & 0xffff) << 16) | (bswap_16(x >> 16));
}



//* FLOAT TO UINT & FLOAT FROM UINT *//
/// ������������ ������ float-�� ������� size � ������ �� 2 � size uint16_t
uint16_t* FloatPtrToUint(const float* n, int size, ByteOrder order)
{
	uint16_t* result = new uint16_t[size * 2];
	for (int i = 0; i < size; ++i)
	{
		//uint32_t tmp = (uint32_t)(*(uint32_t*)&n[i]);
		//result[i * 2] = (uint16_t)(tmp >> 16);
		//result[i * 2 + 1] = (uint16_t)(tmp & 0x0000FFFFuL);

		uint16_t* tmp = new uint16_t[2];

		switch (order)
		{
		case ABCD:
		{
			modbus_set_float_abcd(n[i], tmp);
			break;
		}
		case DCBA:
		{
			modbus_set_float_dcba(n[i], tmp);
			break;
		}
		case BADC:
		{
			modbus_set_float_badc(n[i], tmp);
			break;
		}
		case CDAB:
		{
			modbus_set_float_cdab(n[i], tmp);
			break;
		}
		default:
			modbus_set_float(n[i], tmp);
			break;
		}		
		
		result[i * 2] = tmp[0];
		result[i * 2 + 1] = tmp[1];

		delete[] tmp;
	}
	return result;
}
/// ������������ ������ �� 2 � size uint16_t � ������ float-�� ������� size 
float* UintToFloatPtr(const uint16_t* n, int size, ByteOrder order)
{
	float *result = new float[size];

	for (int i = 0; i < size; ++i)
	{
		uint16_t* tmp = new uint16_t[2];
		tmp[0] = n[i * 2];
		tmp[1] = n[i * 2 + 1];
		switch (order)
		{
		case ABCD:
		{
			result[i] = modbus_get_float_abcd(tmp);
			break;
		}
		case DCBA:
		{
			result[i] = modbus_get_float_dcba(tmp);
			break;
		}
		case BADC:
		{
			result[i] = modbus_get_float_badc(tmp);
			break;
		}
		case CDAB:
		{
			result[i] = modbus_get_float_cdab(tmp);
			break;
		}
		default:
			result[i] = modbus_get_float(tmp);
			break;
		}
		
		delete[] tmp;

		//uint32_t tmp = (((uint32_t)n[i * 2]) << 16) | ((uint32_t)n[i * 2 + 1]);
		//result[i] = (float)(*(float*)&tmp);
	}
	return result;
}

//* 32 bit long TO UINT & 32 bit long  FROM UINT *//
uint16_t* LongPtrToUint(const long* n, int size, ByteOrder order)
{
	uint16_t* result = new uint16_t[size * 2];
	for (int i = 0; i < size; ++i)
	{
		//uint32_t tmp = (uint32_t)(*(uint32_t*)&n[i]);
		//result[i * 2] = (uint16_t)(tmp >> 16);
		//result[i * 2 + 1] = (uint16_t)(tmp & 0x0000FFFFuL);

		//uint16_t* tmp = new uint16_t[2];

		switch (order)
		{
		case ABCD:
		{
			int32_t lval;

			memcpy(&lval, &n[i], sizeof(int32_t));
			lval = htonl(lval);
			result[i * 2] = (uint16_t)(lval >> 16);
			result[i * 2 + 1] = (uint16_t)lval;

			//modbus_set_float_abcd(n[i], tmp);
			break;
		}
		case DCBA:
		{
			int32_t lval;

			memcpy(&lval, &n[i], sizeof(int32_t));
			lval = bswap_32(htonl(lval));
			result[i * 2] = (uint16_t)(lval >> 16);
			result[i * 2 + 1] = (uint16_t)lval;

			//modbus_set_float_dcba(n[i], tmp);
			break;
		}
		case BADC:
		{
			int32_t lval;

			memcpy(&lval, &n[i], sizeof(int32_t));
			lval = htonl(lval);

			result[i * 2] = (uint16_t)bswap_16(lval >> 16);
			result[i * 2 + 1] = (uint16_t)bswap_16(lval & 0xFFFF);

			//modbus_set_float_badc(n[i], tmp);
			break;
		}
		case CDAB:
		{
			int32_t lval;

			memcpy(&lval, &n[i], sizeof(int32_t));
			lval = htonl(lval);

			result[i * 2] = (uint16_t)lval;
			result[i * 2 + 1] = (uint16_t)(lval >> 16);

			//modbus_set_float_cdab(n[i], tmp);
			break;
		}
		default:
			//modbus_set_float(n[i], tmp);
			break;
		}

		//result[i * 2] = tmp[0];
		//result[i * 2 + 1] = tmp[1];

		//delete[] tmp;
	}
	return result;

}
long* UintToLongPtr(const uint16_t* n, int size, ByteOrder order)
{
	long *result = new long[size];

	for (int i = 0; i < size; ++i)
	{
		//uint16_t* tmp = new uint16_t[2];
		//tmp[0] = n[i * 2];
		//tmp[1] = n[i * 2 + 1];
		switch (order)
		{
		case ABCD:
		{
			long f;
			int32_t lval;

			lval = ntohl(((int32_t)n[i * 2] << 16) + n[i * 2 + 1]);
			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_abcd(tmp);
			break;
		}
		case DCBA:
		{
			long f;
			int32_t lval;

			lval = ntohl(bswap_32((((uint32_t)n[i * 2]) << 16) + n[i * 2 + 1]));

			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_dcba(tmp);
			break;
		}
		case BADC:
		{
			long f;
			int32_t lval;

			lval = ntohl((uint32_t)(bswap_16(n[i * 2]) << 16) + bswap_16(n[i * 2 + 1]));

			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_badc(tmp);
			break;
		}
		case CDAB:
		{
			long f;
			int32_t lval;

			lval = ntohl((((uint32_t)n[i * 2 + 1]) << 16) + n[i * 2]);


			memcpy(&f, &lval, sizeof(long));
			result[i] = f;
			//result[i] = modbus_get_float_cdab(tmp);
			break;
		}
		default:
			//result[i] = modbus_get_float(tmp);
			break;
		}

		//delete[] tmp;

	}
	return result;
}


int previousmain()
{
	modbus_t *ctx;
	//uint16_t tab_reg[32];

	//ctx = modbus_new_tcp("127.0.0.1", 3487);
	//ctx = modbus_new_tcp("192.168.10.84", 3487);
	ctx = modbus_new_tcp("192.168.10.61", 49004);

	//modbus_set_debug(ctx, TRUE);
	modbus_set_response_timeout(ctx, 0, 999999);

	if (modbus_connect(ctx) == -1)
	{
		std::cout << "Connection failed" << std::endl;
		modbus_free(ctx);
		return -1;
	}
	std::cout << "Connection complete" << std::endl;

	/* Allocate and initialize the different memory spaces */
	int nb = ADDRESS_END - ADDRESS_START;

	uint8_t *tab_rq_bits = (uint8_t *)malloc(nb * sizeof(uint8_t));
	memset(tab_rq_bits, 0, nb * sizeof(uint8_t));

	uint8_t *tab_rp_bits = (uint8_t *)malloc(nb * sizeof(uint8_t));
	memset(tab_rp_bits, 0, nb * sizeof(uint8_t));

	for (int addr = ADDRESS_START; addr < ADDRESS_END; ++addr)
	{
		tab_rq_bits[addr] = (uint16_t)1;
		tab_rp_bits[0] = (uint16_t)0;
	}

	/*
	for (int addr = ADDRESS_START; addr < ADDRESS_END; ++addr)
	{
	nb = ADDRESS_END - addr;
	// WRITE BIT //
	int rc = modbus_write_bit(ctx, addr, tab_rq_bits[addr]);
	if (rc != 1)
	{
	std::cout << "ERROR modbus_write_bit  "<< rc << std::endl;
	std::cout << "Address =  " << addr << " value = " << tab_rq_bits[addr] << std::endl;
	std::cout << modbus_strerror(errno) << std::endl;
	}
	}
	//*/
	//std::cout << "start sleep"<< std::endl;
	//Sleep(5000);
	//std::cout << "end sleep"<< std::endl;

	int mistaces = 0;
	for (int addr = ADDRESS_START; addr < ADDRESS_END; ++addr)
	{
		int rc = modbus_read_bits(ctx, addr, 1, tab_rp_bits);
		if (rc != 1 || tab_rq_bits[addr] != tab_rp_bits[0])
		{
			std::cout << "ERROR modbus_read_bits single " << rc << std::endl;
			std::cout << "Address =  " << addr << std::endl;
			mistaces++;
		}
	}

	if (mistaces == 0)
		std::cout << "SUCCES " << std::endl;

	modbus_close(ctx);
	std::cout << "connection close" << std::endl;

	modbus_free(ctx);

	/* Free the memory */
	free(tab_rq_bits);
	free(tab_rp_bits);

	system("pause");
	return 0;
}



int initRemoteSlave(const float *testFloatMass)
{
	//Type tp = TCP;
	Type tp = RTU;

	int rc = 0;
	uint16_t* toWrite = FloatPtrToUint(testFloatMass, FLOAT_ARRAY_DIMENSION, ABCD);

	modbus_t *ctx;

	if (tp == TCP) {
		ctx = modbus_new_tcp("192.168.10.27", 1502);
	}
	else 
	{
		//ctx = modbus_new_rtu("\\\\.\\COM5", 19200, 'N', 8, 1);
		//ctx = modbus_new_rtu("\\\\.\\COM3", 57600, 'N', 8, 1);
		ctx = modbus_new_rtu("\\\\.\\COM1", 57600, 'N', 8, 1);

	}
	if (ctx == NULL) {
		fprintf(stderr, "Unable to allocate libmodbus context\n");
		return -1;
	}
	modbus_set_debug(ctx, TRUE);
	modbus_set_response_timeout(ctx, 0, 999999);

	if (tp == RTU)
	{
		//modbus_set_slave(ctx, 2);
		//modbus_set_slave(ctx, 6);
		modbus_set_slave(ctx, 17);
	}
	
	if (modbus_connect(ctx) == -1)
	{
		std::cout << "Connection with remote slave failed" << std::endl;
		modbus_free(ctx);
		return -1;
	}
	
	std::cout << "Connection with remote slave complete" << std::endl;

	int nb = 2;
	uint16_t* tab_reg = new uint16_t[nb * sizeof(uint16_t)];

	/*
	//rc = modbus_read_registers(ctx, 1, nb, tab_reg); //#3

	while (1)
	{
		for (size_t i = 0; i < 8; i += 2)
		{
			long f;
			int32_t ity;
			rc = modbus_read_input_registers(ctx, i, nb, tab_reg); //#4

			ity = ntohl(bswap_32((((int32_t)tab_reg[0]) << 16) + tab_reg[1]));
			memcpy(&f, &ity, sizeof(long));


			std::cout << "adr "<< i <<"  " << f << std::endl;

		}
		std::cout << std::endl;

	}*/
	

	rc = modbus_write_registers(ctx, 0, FLOAT_ARRAY_DIMENSION * 2, toWrite);
	
	modbus_close(ctx);
	modbus_free(ctx);
	delete [] toWrite;
	if (rc < 0)
	{
		std::cout << "error write to remote slave" << std::endl;
		return rc;
	}
	return 0;
}

int readFromActiveXAndTestFloats(const float *testFloatMass)
{
	modbus_t *ctx;
	ctx = modbus_new_tcp("192.168.0.102", 3487);
	modbus_set_debug(ctx, TRUE);
	modbus_set_response_timeout(ctx, 0, 999999);

	int rc = modbus_connect(ctx);
	if (rc == -1)
	{
		std::cout << "Connection with ActiveX slave failed" << std::endl;
		modbus_free(ctx);
		return -1;
	}
	std::cout << "Connection with ActiveX slave complete" << std::endl;

	uint16_t* tab_reg = new uint16_t[FLOAT_ARRAY_DIMENSION * 2];
	bool succFlag = false;
	while (!succFlag)
	{
		rc = modbus_read_registers(ctx, 0, FLOAT_ARRAY_DIMENSION * 2, tab_reg);
		
		if (rc < 0)
		{
			std::cout << "error read" << std::endl;
			delete[] tab_reg;
			modbus_close(ctx);
			modbus_free(ctx);
			return -1;
		}
		float* getArray = UintToFloatPtr(tab_reg, FLOAT_ARRAY_DIMENSION, ABCD);

		for (size_t i = 0; i < FLOAT_ARRAY_DIMENSION; i++)
		{
			std::cout << " " << getArray[i];
		}
		std::cout << std::endl;


		int counter = 0;
		for (int i = 0; i < FLOAT_ARRAY_DIMENSION; ++i)
		{
			if (getArray[i] != testFloatMass[i])
			{
				break;
			}
			counter++;
		}

		if (counter == FLOAT_ARRAY_DIMENSION)
		{
			std::cout << "Succes" << std::endl;
			succFlag = true;
		}
		delete[] getArray;
	}

	modbus_close(ctx);
	modbus_free(ctx);
	delete[] tab_reg;
	return 0;
}

int readFromActiveXAndTestFLongs( long *testFloatMass)
{

	modbus_t *ctx;
	ctx = modbus_new_tcp("192.168.10.57", 3487);
	modbus_set_debug(ctx, TRUE);
	modbus_set_response_timeout(ctx, 0, 999999);

	int rc = modbus_connect(ctx);
	if (rc == -1)
	{
		std::cout << "Connection with ActiveX slave failed" << std::endl;
		modbus_free(ctx);
		return -1;
	}
	std::cout << "Connection with ActiveX slave complete" << std::endl;

	uint16_t* tab_reg = new uint16_t[FLOAT_ARRAY_DIMENSION * 2];
	bool succFlag = false;
	int adr = 0;
	while (!succFlag)
	{
		for (int o = 0; o < FLOAT_ARRAY_DIMENSION; o++)
		{
			testFloatMass[o] = (long)(adr * 40 + o);
		}
		uint16_t* tab_reg_write = LongPtrToUint(testFloatMass, FLOAT_ARRAY_DIMENSION, ABCD);


		//rc = modbus_write_registers(ctx, adr, FLOAT_ARRAY_DIMENSION * 2, tab_reg_write);
		//delete tab_reg_write;
		
		rc = modbus_read_registers(ctx, adr, FLOAT_ARRAY_DIMENSION * 2, tab_reg);

		adr += 2;
		if (adr == 50)
		{
			adr = 0;
		}

		if (rc < 0)
		{
			std::cout << "error read" << std::endl;
			delete[] tab_reg;
			modbus_close(ctx);
			modbus_free(ctx);
			return -1;
		}
		long* getArray = UintToLongPtr(tab_reg, FLOAT_ARRAY_DIMENSION, ABCD);

		for (size_t i = 0; i < FLOAT_ARRAY_DIMENSION; i++)
		{
			std::cout << " " << getArray[i];
		}
		std::cout << std::endl;

		int counter = 0;
		for (int i = 0; i < FLOAT_ARRAY_DIMENSION; ++i)
		{
			if (getArray[i] != testFloatMass[i])
			{
				break;
			}
			counter++;
		}
		
		if (counter == FLOAT_ARRAY_DIMENSION)
		{
			std::cout << "Succes" << std::endl;
			//succFlag = true;
		}
		delete[] getArray;
	}

	modbus_close(ctx);
	modbus_free(ctx);
	delete[] tab_reg;
	return 0;
}

int testwork()
{
	modbus_t *ctx;
	ctx = modbus_new_tcp("192.168.10.61", 49004);
	modbus_set_debug(ctx, TRUE);
	modbus_set_response_timeout(ctx, 0, 999999);
	modbus_set_slave(ctx, 4);
	int rc = modbus_connect(ctx);
	if (rc == -1)
	{
		std::cout << "Connection with ActiveX slave failed" << std::endl;
		modbus_free(ctx);
		return -1;
	}

	std::cout << "Connection with ActiveX slave complete" << std::endl;
	
	uint16_t* tab_reg = new uint16_t[5];
	rc = modbus_read_registers(ctx, 0, 5, tab_reg);
	//float* getArray = UintToFloatPtr(tab_reg, FLOAT_ARRAY_DIMENSION, ABCD);
	long* getArray = UintToLongPtr(tab_reg, 5, DCBA);
	for (int i = 0; i < 5; ++i)
	{
		std::cout << getArray[i] <<"  ";
	}


	modbus_close(ctx);
	modbus_free(ctx);
	delete[] tab_reg;
	return 0;
}


int getIni(ParamS &params)
{
	std::string str;
	std::vector<std::string> v1;
	std::ifstream file("params.ini");
	// Loop until end-of-file (Ctrl-Z) is input, store each sentence in a vector.
	// Default delimiter is the newline character.
	
	while (getline(file, str)) {
		v1.push_back(str);
	}

	std::cout << "The following input was stored with newline delimiter:" << std::endl;
	for (const auto& p : v1) {
		std::cout << p << std::endl;
	}

	std::cin.clear();

	return 1;
}
int _tmain(int argc, _TCHAR* argv[])
{
	ParamS params;

	//getIni(params);
	/*
	float *testFloatMass = new float[FLOAT_ARRAY_DIMENSION];
	for (int i = 0; i < FLOAT_ARRAY_DIMENSION; ++i)
	{
		testFloatMass[i] = (float)(i + 0.5);
	}
	//*/
	
	/*long *testFloatMass = new long[FLOAT_ARRAY_DIMENSION];
	for (int i = 0; i < FLOAT_ARRAY_DIMENSION; ++i)
	{
		testFloatMass[i] = (long)5;
	}
	//*/

	//int *a = new int(1000);

	/*for (size_t i = 0; i < FLOAT_ARRAY_DIMENSION; i++)
	{
		std::cout << " " << testFloatMass[i];
	}
	std::cout << std::endl;

	int rc = 1;
	 rc = initRemoteSlave(testFloatMass);
	if (rc >= 0)
	{
		testwork();
	}//*/
	testwork();
	//delete[] testFloatMass;
	system("pause");
	return 0;
}

