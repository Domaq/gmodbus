// libmodbustestserver.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include "stdafx.h"

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#include <string.h>

#include "libmodbus/headers/modbus.h"
#pragma comment(lib, "libmodbus/modbus.lib")


#if defined(_WIN32)
#pragma comment(lib, "WS2_32.lib")
#include <ws2tcpip.h>
#else
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#define NB_CONNECTION    5

//#define _MANYUP_
#define SERVER_ID         17

static modbus_t *ctx = NULL;
static modbus_mapping_t *mb_mapping;

static int server_socket = -1;

typedef enum {
	TCP,
	RTU
} Type;

static void close_sigint(int dummy)
{
	if (server_socket != -1)
	{
		closesocket(server_socket);
	}
	modbus_free(ctx);
	modbus_mapping_free(mb_mapping);

	exit(dummy);
}

int manyUpConnectionTCP()
{
	uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
	int master_socket;
	int rc;
	fd_set refset;
	fd_set rdset;
	/* Maximum file descriptor number */
	int fdmax;

	ctx = modbus_new_tcp("192.168.10.27", 1502);
	modbus_set_debug(ctx, TRUE);
	server_socket = modbus_tcp_listen(ctx, NB_CONNECTION);
	if (server_socket == -1)
	{
		std::cout << "Unable to listen TCP connection " << std::endl;
		modbus_free(ctx);
		return -1;
	}

	signal(SIGINT, close_sigint);

	/* Clear the reference set of socket */
	FD_ZERO(&refset);
	/* Add the server socket */
	FD_SET(server_socket, &refset);

	/* Keep track of the max file descriptor */
	fdmax = server_socket;

	for (;;)
	{
		rdset = refset;
		if (select(fdmax + 1, &rdset, NULL, NULL, NULL) == -1)
		{
			perror("Server select() failure.");
			close_sigint(1);
		}

		/* Run through the existing connections looking for data to be
		* read */
		for (master_socket = 0; master_socket <= fdmax; master_socket++)
		{

			if (!FD_ISSET(master_socket, &rdset))
			{
				continue;
			}

			if (master_socket == server_socket)
			{
				/* A client is asking a new connection */
				socklen_t addrlen;
				struct sockaddr_in clientaddr;
				int newfd;

				/* Handle new connections */
				addrlen = sizeof(clientaddr);
				memset(&clientaddr, 0, sizeof(clientaddr));
				newfd = accept(server_socket, (struct sockaddr *)&clientaddr, &addrlen);
				if (newfd == -1) {
					perror("Server accept() error");
				}
				else {
					FD_SET(newfd, &refset);

					if (newfd > fdmax)
					{
						/* Keep track of the maximum */
						fdmax = newfd;
					}
					//printf("New connection from %s:%d on socket %d\n",inet_ntoa(clientaddr.sin_addr), clientaddr.sin_port, newfd);
				}
			}
			else
			{
				modbus_set_socket(ctx, master_socket);

				rc = modbus_receive(ctx, query);
				if (rc > 0)
				{
					modbus_reply(ctx, query, rc, mb_mapping);
				}
				else if (rc == -1)
				{
					/* This example server in ended on connection closing or
					* any errors. */
					printf("Connection closed on socket %d\n", master_socket);
					closesocket(master_socket);

					/* Remove from reference set */
					FD_CLR(master_socket, &refset);

					if (master_socket == fdmax)
					{
						fdmax--;
					}
				}
			}
		}
	}
	return 0;
}

int oneConnection(Type tp)
{
	int queryln = 0;
	int s = -1;
	int rc;

	if (tp == TCP)
	{
		ctx = modbus_new_tcp("192.168.10.55", 1502);
		queryln = MODBUS_TCP_MAX_ADU_LENGTH;
	}
	else 
	{
		ctx = modbus_new_rtu("\\\\.\\COM15", 9600, 'N', 8, 1);
		queryln = MODBUS_RTU_MAX_ADU_LENGTH;
		modbus_set_slave(ctx, SERVER_ID);
	}
	modbus_set_debug(ctx, TRUE);
	uint8_t* query = new uint8_t[queryln];
	//header_length = modbus_get_header_length(ctx);

	if (tp == TCP)
	{
		s = modbus_tcp_listen(ctx, 1);
		modbus_tcp_accept(ctx, &s);
	}
	else
	{
		rc = modbus_connect(ctx);
		if (rc == -1)
		{
			fprintf(stderr, "Unable to connect %s\n", modbus_strerror(errno));
			modbus_free(ctx);
			return -1;
		}
	}


	for (;;)
	{
		do
		{
			rc = modbus_receive(ctx, query);
			/* Filtered queries return 0 */
		} while (rc == 0);

		/* The connection is not closed on errors which require on reply such as
		bad CRC in RTU. */
		if (rc == -1 && errno != EMBBADCRC) {
			/* Quit */
			break;
		}

		rc = modbus_reply(ctx, query, rc, mb_mapping);
		if (rc == -1)
		{
			break;
		}
	}

	printf("Quit the loop: %s\n", modbus_strerror(errno));

	if (tp == TCP)
	{
		if (s != -1)
		{
			closesocket(s);
		}
	}
	delete[] query;

	/* For RTU */
	modbus_close(ctx);
	modbus_free(ctx);

	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	mb_mapping = modbus_mapping_new(MODBUS_MAX_READ_BITS, MODBUS_MAX_WRITE_BITS, MODBUS_MAX_READ_REGISTERS, MODBUS_MAX_WRITE_REGISTERS);
	if (mb_mapping == NULL)
	{
		std::cout << "Failed to allocate the mapping: "<<  modbus_strerror(errno) << std::endl;
		return -1;
	}
	bool upstatus = false;

#ifdef _MANYUP_
	upstatus = true;
#endif

	int rc = 0;
	if (upstatus)
	{
		rc = manyUpConnectionTCP();
	}
	else
	{
		while (1)
		{
			rc = oneConnection(TCP);
			//rc = oneConnection(RTU);
		}
		modbus_mapping_free(mb_mapping);
	}

	system("pause");
	return rc;
}

