#Описание

Протокол Modbus содержит в себе много вариантов использования, например, последовательный RTU или Ehternet TCP.
Протокол описывает поведение двух сущностей, а именно: Slave и Master. С подробным описанием протокола можно ознакомиться на [wiki] 

Данная реализация протокола адаптирует технологию и агрегирует её в один ActiveX Component, который можно использовать, и как Slave, и как Master в Вашей программе, поддерживающей COM архитектуру.

Код компонента не реализует сам протокол Modbus, а использует API библиотеки [libmodbus] v 3.14 и создан для возможности внедрения протокола Modbus в промышленное программное обеспечение, архитектура которого основана на COM.

## Структура

Чтобы изолировать реализацию работы с API в модуле был выделен класс ModbusAdapter, который обеспечивает взаимодействие с [libmodbus] v 3.14.

Можно выделить следующие компоненты ActiveX Component-а:

- Карта регистров
- Slave (local)
- Master (for local Slave)
- Master (for remote Slaves)

### Карта регистров

Карта регистров содержит в себе всю информацию, которая подлежит обмену между устройствами в сети, которые работают по данному протоколу. 
Для корректной работы модуля необходимо, чтобы все устройства имели семантически одинаковую карту регистров, а в процессе работы устройств происходил лишь обмен конкретными значениями из карт, расположенных в каждом конкретном устройстве. 

В качестве карты регистров используется реализация карты регистров из [libmodbus] v 3.14.

```
typedef struct {
    int nb_bits;
    int start_bits;
    int nb_input_bits;
    int start_input_bits;
    int nb_input_registers;
    int start_input_registers;
    int nb_registers;
    int start_registers;
    uint8_t *tab_bits;
    uint8_t *tab_input_bits;
    uint16_t *tab_input_registers;
    uint16_t *tab_registers;
} modbus_mapping_t;

```
Все описанные ниже компоненты работают с одной картой, являющейся static членом компонента.

### Slave (local)

В компоненте содержится static ModbusAdapter, реализующий локальный Slave. Локальный Slave поддерживает до 9 TCP(IPv4) соединений для выступающих в 
роли Master устройств. Другими словами, в процессе работы компонента может быть установлено до девяти одновременных TCP соединений, в процессе работы которых
local Slave будет принимать пакеты и формировать ответы в соответствии с протоколом ModBus.

Проинициализировать Slave в коде Вашего приложения очень просто. После того как ActiveX Component будет добавлен на форму, 
необходимо сделать следующие вызовы: (код VBS)

```
Function CreateSlaveTest()

	'проверка запуска slave
	ip = "127.0.0.1"
	port = 3487
	CreateSlaveTest = initSlave(ip, port)
	
End Function

Function initSlave (ip , port )
	initSlave = CBool(1)
	result = Form.GModbus1.initConnectionAsSlave (ip,port)
	If (result <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		resultstr = "Ошибка запуска Slave. Код ошибки: " & CStr(result)
		initSlave = CBool(0)
	End If
End Function

```

Как можно заметить, инициализация Slave сопровождается двумя параметрами: порт и IP адрес. Это параметры Slave для установки TCP(IPv4) соединений.

Подробное описание вызываемых методов можно изучить в разделе /docs/API.md

### Master (for local Slave)

В компоненте содержится static ModbusAdapter, реализующий Master, который осуществляет взаимодействие с локальным Slave из предыдущего пункта.
Так как карта изолирована, и работать с ней может только Slave был внедрён подобный механизм. По своей сути этот адаптер эмулирует некоторое устройство в сети, 
которое выступает в роли Master и отправляет запросы на Slave, с целью записать или считать данные. Это необходимо, например, если необходимо считать данные или записать их в карту из кода программного обеспечения, 
использующего компонент.

Пример (код VBA\VBS)

```
Function ReadFromMapTest()

	ReadFromMapTest = CBool(1)
	OutMess "Переходим к чтению"
	' *** Чтение значений из карты *** '
	addr = 0	'адрес регистра, с которого начинаем чтение
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	bo = 0		'ByteOrder (ABCD = 0)
	dt = 3 		'тип данных возвращаемого значения (LONGS)
	ft = 3 		'FunctionType (Read_Holding_Registers = 3)

	readenArray = Form.ReadRegistrsWithLongTypeFromMap(addr, nb, ft, bo, dt)
	
	If IsEmpty(readenArray) Then
				OutMess  "получили пустой массив"
	End If

	If Not IsEmpty(readenArray) Then
		For i = 0 To UBound(readenArray)
			If readenArray(i,0) <> arrTemp(i) Then
				OutMess  "Проверка не пройдена на значении с индексом" & CStr(i)
				ReadFromMapTest = CBool(0)
			End If
		Next
	End If
	
	OutMess "Чтение окончено"

End Function

Public Function ReadRegistrsWithLongTypeFromMap(addr, nb, ft, bo, dt) As Variant
    Dim result As Variant
    
    result = GModbus1.modbusReadFromServer(addr, nb, ft, bo, dt)
    If (IsEmpty(result)) Then
        ReadRegistrsWithLongTypeFromMap = result
        Exit Function
    End If
    Dim resultforVB() As Variant
    ReDim resultforVB(0 To UBound(result), 0)
    Dim i As Integer
    
    For i = 0 To UBound(result)
        resultforVB(i, 0) = result(i)
    Next i
    
    ReadRegistrsWithLongTypeFromMap = resultforVB
End Function
```

### Master (for remote Slaves)

В компоненте предполагается хранение произвольного количества адаптеров, которые выступают в роли Master и обмениваются данными 
с другими устройствами в сети (TCP или RTU), которые выступают в роли Slave.
Предполагается, что remote Slave содержит в себе семантически идентичную карту регистров.

Схема использования:

	|--------------------------|	   	                            		   	|-------------------------|
	|	устройство A1 (Master) |									   	  	    |  устройство Б1 (Salve)  |
	|	 не имеет доступа к    |<											   >|  Строит оценку типа Б1  |
    |    устройствам типа Б    | \											  /	|-------------------------|
	|	Запрашивает оценку 	   | TCP	    	                             /      		               
	|	   типа Б1 и Б2		   |   \   	                                   TCP   	                       
	|--------------------------|	\  	                                   /    |-------------------------|
									 \									  /	   >|  устройство Б2 (Salve)  |
									  \					     			 /	  / |  Строит оценку типа Б2  |
	|--------------------------|	   >|------------------------------|<	 /  |-------------------------|
	|	устройство A2 (Master) |	   	|   GModbus.                   |    /							   
	|	не имеет доступа к     |	   	|   GModbus.Slave обеспечивает |   RTU							   
	|	устройствам типа Б	   |<--TCP->|	обмен с устройствами типа А|  / 	|-------------------------|
	|	Запрашивает оценку     |	   	|                              | /		|  устройство Б3 (Salve)  |
	|	   типа Б1 и Б3	       |	   	|   GModbus.Mastrs обеспечивают|<   	|  Строит оценку типа Б3  |
	|--------------------------|	   >|   обмен с устройствами типа Б|<-RTU-->|-------------------------|
									  /	|							   |								   
	|--------------------------|  	 /  |   Актуальная информация      |              					   
	|	устройство A2 (Master) | 	/	|	запрашивается у устройств  |	    |-------------------------|
	|	не имеет доступа к	   |  TCP	|   типа Б. Сохраняется в Map. |        |  устройство Б4 (Salve)  |
	|	устройствам типа Б	   |  /		|	По запросу предоставляется |<--TCP->|  Строит оценку типа Б4  |
	|	Запрашивает оценку	   | / 	   	|   устройствам типа А         |        |-------------------------|
	|		 типа Б4 и Б2	   |<	   	|------------------------------|     							   
	|--------------------------|	   								 	   								   


Для того чтобы использовать адаптеры, выступающие в роли Masters for remote Slave необходимо:

- Сообщить компоненту, сколько таких соединений будет поддерживаться.
- Установить каждое соединение.
- Взаимодействие с remote Slave проводить по индексу соединения из пред. пункта (начиная с 0).
- Запускать ReConnect  в случае сбоя соединения.

Итак, по порядку:

Следующий код демонстрирует первые два пункта. Инициализируется одно соединение. Далее добавляется мастер, который будет поддерживать RTU соединение с заданными параметрами. (код VBS)

```
Function AddMasterToRemoteSlaveRTUTest()

	AddMasterToRemoteSlaveRTUTest = CBool(1)

	rc = Form.GModbus1.modbusRemoteMastersInitialization(1)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & "Не удалось проинициализировать Мастера Код ошибки: " & CStr(rc)
		AddMasterToRemoteSlaveRTUTest = CBool(0)
		Exit Function
	End If	
	
	OutMess  "Пробуем подключение к удалённому Slave по RTU"
	device = "\\.\COM15"
	servID = 17
	baud = 9600

	OutMess  "Параметры: device = " & CStr(device) 
	OutMess  "servID = " & CStr(servID)
	OutMess  "baud = " & CStr(baud)
		
	rc = Form.GModbus1.modbusAddMasterToRemoteSlaveRTU(device, servID, baud)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		AddMasterToRemoteSlaveRTUTest = CBool(0)
	End If	
	
End Function
```	


При взаимодействии с remote Slaves происходит частичная синхронизация карт на локальном и удалённом Slave. Поддерживается два режима работы:

- запись регистров из local Slave на remote Slave.
- чтение регистров из remote Slave в local Slave.

В первом случае можно выделить следующие шаги.

- local Master запрашивает у local Slave некоторое количество регистров.
- local Slave возвращает данные из ка карты регистров.
- Данные сохраняются в буферный массив.
- remote Master отправляет запрос на запись, содержащий в себе временный массив, на remote Slave.	
- remote Slave обновляет карту.

Таким образом происходит синхронизация части карты на удалённом Slave в соответствии с картой в компоненте.	

Чтение регистров происходит аналогично, но в обратном порядке. Пример кода, использующего эту функциональность(VBS)

```
Function WriteToRemoteServerTest()

	WriteToRemoteServerTest = CBool(1)
	OutMess "Переходим к записи на Remote Salve"
	' *** Запись значений в Remote Salve *** '
	addr = 0	'адрес регистра, с которого начинаем запись
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	index = 0	'индекс соединения (в нашем случае оно одно, поэтому индекс 0)
	rc = Form.GModbus1.modbusWriteRegistersToRemoteSlave(addr, nb, index)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		WriteToRemoteServerTest = CBool(0)
	End If
	OutMess "Запись окончена"
	
End Function

Function ReadFromRemoteSalveTest()

	ReadFromRemoteSalveTest = CBool(1)
	OutMess "Переходим к чтению из Remote Salve"
	' *** Чтение значений из Remote Salve *** '
	addr = 0	'адрес регистра, с которого начинаем чтение
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	index = 0	'индекс соединения (в нашем случае оно одно, поэтому индекс 0)
	
	rc = Form.GModbus1.modbusReadRegistersFromRemoteSlave(addr, nb, index)
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		ReadFromRemoteSalveTest = CBool(0)
	End If	
	
	OutMess "Чтение окончено"

End Function
```

Для обновления соединения вызывается метод

```
GModbus1.modbusMastersUpdateConections()
```

Этот метод перебирает все адаптеры для remote Slaves и, в случае, отсутствия соединения восстанавливает его.

[wiki]: https://en.wikipedia.org/wiki/Modbus
[libmodbus]: http://libmodbus.org
