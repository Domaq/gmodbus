# Описание API
Этот документ описывает API, которое предоставляет ActiveX Component GModbus. 

## Введённые перечисления

Так как, COM архитектура позволяет запускать компоненты написанные на C++ в относительно произвольной среду, на интерфейс этих компонентов накладываются некоторые ограничения. 
Многие методы в качестве параметра принимают, так называемые, флаги, диапазон значений которых ограничен. Другими словами, методы принимают на вход переменную пользовательского типа, которая является перечислением.
В данной реализации, тип параметров интерфейса, выполняющих роль флага, установлен как int. Ниже приведены, перечисления, которые используются в коде компонента, для упрощения объяснения смысла параметров в будущем описании интерфейса.

### ResultStatuses
```
typedef enum {
	_RC_UNKNOWN_ERROR_ = -1,
	_RC_SLAVA_ALREADY_EXIST_ERROR_ = -2,
	_RC_MORE_THEN_MAX_THREADS_ERROR = -3,
	_RC_SLAVE_OUT_OF_TIME_ERROR_ = -4,
	_RC_MAP_ALLOCATION_ERROR_ = -5,
	_RC_INCORRECT_REMOTE_MASTERS_NUMBER_ERROR_ = -6,
	_RC_INCORRECT_FUNCTION_TYPE_ERROR_ = -7,
	_RC_INCORRECT_INPUT_DATA_ERROR_ = -8,
	_RC_NOT_NULL_ERROR_ = -9,
	_RC_CONNECTION_ERROR = -10,
	_RC_SUCCES_ = 1
} ResultStatuses;
```
Почти все интерфейсные методы возвращают переменную типа int, которая определена на приведённом выше диапазоне кодов возврата. Эти коды возврата можно использовать для корректной обработки run-time errors в Вашем приложении.  

### ConnectionType
```
typedef enum{
	TCP,
	RTU,
	NOTYPE
} ConnectionType;
```
Перечисление ConnectionType определяет допустимые типы подключения, которые может реализовать ModbusMaster. NOTYPE - дефолтное значение, необходимое для отслеживания ошибок.

### DataType
```
typedef enum{
	BITS,
	REGISTRS, // 32-bit values
	FLOATS, // 32-bit values
	LONGS, // 32-bit values
	NODATATYPE
} DataType;
```
Перечисление DataType определяет допустимые типы данных, которые могут быть переданы при использовании ModbusMaster. NODATATYPE - дефолтное значение, необходимое для отслеживания ошибок.

### ByteOrder
```
typedef enum {
	ABCD = 0,
	DCBA = 1,
	BADC = 2,
	CDAB = 3,
	DEFAULT = -1
} ByteOrder;
```
Перечисление ByteOrder определяет допустимые порядки следования байт в 32-битных значениях, которые могут быть переданы при использовании ModbusMaster.

### FunctionType
```
typedef enum {
	Read_Coil_Status = 1,
	Read_Discrete_Inputs = 2,
	Read_Holding_Registers = 3,
	Read_Input_Registers = 4,
	Write_Multiple_Coils = 15,
	Write_Multiple_Registers = 16
} FunctionType;
```
Перечисление FunctionType определяет реализуемые компонентом функции протокола Modbus (см [wiki]).

## Интерфейс

Компонент предоставляет следующие интерфейсные вызовы:

```
	//* MPDBUS INFO *//

[id(1)] BSTR getConnectionStatusInfo();

	//* MPDBUS SLAVE *//

[id(2)] int initConnectionAsSlave(BSTR ip, int port);	
[id(3)] void stopSlave();

	//* MPDBUS MASTER *//

[id(4)] int modbusWriteToServer(int addr, int nb, int ft, int bo, VARIANT Data);
[id(5)] VARIANT modbusReadFromServer(int addr, int nb, int ft, int bo, int dt); 
			
[id(6)] int modbusReadRegistersFromRemoteSlave(int addr, int nb, int rmtSlaveIndex);
[id(7)] int modbusWriteRegistersToRemoteSlave(int addr, int nb, int rmtSlaveIndex); 

[id(8)] int modbusAddMasterToRemoteSlaveRTU(BSTR device, int servId, int baud); 
[id(9)] int modbusAddMasterToRemoteSlaveTCP(BSTR ip, int port); 

[id(10)] int modbusMastersUpdateConections(); 
[id(11)] int modbusRemoteMastersInitialization(int nb); 
```

### getConnectionStatusInfo
```
BSTR CGModbusCtrl::getConnectionStatusInfo()
```
Метод возвращает менаинформацию о состоянии компонента. Вся метаинформация, например об ошибках, собирается в некоторый буфер. В результате вызова этого метода информация возвращается в виде BSTR, а буфер чистится. 
Рекомендуется использовать для логирования процесса.

### initConnectionAsSlave
```
int CGModbusCtrl::initConnectionAsSlave(LPCTSTR ip, int port)
```
Метод инициализирует static ModbusAdapter, реализующий локальный Slave. Локальный Slave поддерживает до 9 TCP(IPv4) соединений для выступающих в 
роли Master устройств. Другими словами, в процессе работы компонента может быть установлено до девяти одновременных TCP соединений, в процессе работы которых
local Slave будет принимать пакеты и формировать ответы в соответствии с протоколом ModBus. В методе наблюдаются следующие этапы:

- Cоздаёт локальный slave и слушает эфир на предмет новых подключений
- Cоздаёт локального мастера для общения с локальным slave
- Устанавливает соединение между локальными мастером и slave

##### Параметры
- LPCTSTR ip : IPv4 адрес Salve. По этому адресу должны подключаться к компоненту устройства, выступающие в роли Master.
- int port : порт Salve. По этому порту должны подключаться к компоненту устройства, выступающие в роли Master.

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo

```
	rc = Form.GModbus1.modbusAddMasterToRemoteSlaveRTU(device, servID, baud)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		AddMasterToRemoteSlaveRTUTest = CBool(0)
	End If	
```

### stopSlave
```
void CGModbusCtrl::stopSlave()
```
Метод останавливает работу Slave, действуя в соответствии со следующими этапами.

- Останавливает работу локального slave
- Удаляет локального мастера
- Очищает массив мастеров, необходимых для работы с удалёными slave

```
Sub stopSlave ()
	Form.GModbus1.stopSlave()
	res = Form.InfoModbus()
	OutMess res
End sub
```

### modbusWriteToServer
```
int CGModbusCtrl::modbusWriteToServer(int addr, int nb, int ft, int bo, const VARIANT &Data)
```
Метод реализует запись в карту набор значений, переданных в  VARIANT (FLOAT_PTR ИЛИ LONG_PTR)

##### Параметры
- int addr : адрес регистра в карте регистров, с которого начнётся запись.
- int nb   : количество байт записи
- int ft   : значение из перечисления FunctionType. На данный момент эта функция поддерживает только FunctionType::Write_Multiple_Registers.
- int bo   : значение из перечисления ByteOrder.
- VARIANT &Data : массив значений, которые будут записаны в карту. На данный момент эта функция поддерживает только массив значений типа Float и Long

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo

```
Function WriteToMapTest()

	WriteToMapTest = CBool(1)
	OutMess "Переходим к записи"
	' *** Запись значений в карту *** '
	addr = 0	'адрес регистра, с которого начинаем запись
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	ft = 16 	'FunctionType (Write_Multiple_Registers = 16)
	bo = 0		'ByteOrder (ABCD = 0)
	
	rc = Form.WriteRegistrsWithLongTypeToMap(arrTemp,addr, nb, ft, bo)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		WriteToMapTest = CBool(0)
	End If
	OutMess "Запись окончена"
	
End Function

Public Function WriteRegistrsWithLongTypeToMap(m_arr, addr, nb, ft, bo) As Integer
	Dim arrTemp() As Long
	ReDim arrTemp(UBound(m_arr))
	Dim i As Long
	For i = 0 To UBound(m_arr)
		arrTemp(i) = m_arr(i)
	Next i
	WriteRegistrsWithLongTypeToMap = GModbus1.modbusWriteRegistersToServer(addr, nb, ft, bo, arrTemp)
End Function
```

### modbusReadFromServer
```
VARIANT CGModbusCtrl::modbusReadFromServer(int addr, int nb, int ft, int bo, int dt)
```
Метод читает из карты набор nb/2 32-bit значений по адресу addr и возвращает их в формате VARIANT  

##### Параметры
- int addr : адрес регистра в карте регистров, с которого начнётся чтение.
- int nb   : количество байт чтения
- int ft   : значение из перечисления FunctionType. На данный момент эта функция поддерживает только FunctionType::Read_Holding_Registers и FunctionType::Read_Input_Registers
- int bo   : значение из перечисления ByteOrder
- int dt   : значение из перечисления DataType. На данный момент эта функция поддерживает только DataType::FLOATS и DataType::LONGS

##### Возвращаемое значение
Возвращает VARIANT, который содержит в себе массив данных типа dt. В случае ошибок, вернётся VT_EMPTY, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo

```
Function ReadFromMapTest()

	ReadFromMapTest = CBool(1)
	OutMess "Переходим к чтению"
	' *** Чтение значений из карты *** '
	addr = 0	'адрес регистра, с которого начинаем чтение
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	bo = 0		'ByteOrder (ABCD = 0)
	dt = 3 		'тип данных возвращаемого значения (LONGS)
	ft = 3 		'FunctionType (Read_Holding_Registers = 3)

	readenArray = Form.ReadRegistrsWithLongTypeFromMap(addr, nb, ft, bo, dt)
	
	If IsEmpty(readenArray) Then
				OutMess  "получили пустой массив"
	End If

	If Not IsEmpty(readenArray) Then
		For i = 0 To UBound(readenArray)
			If readenArray(i,0) <> arrTemp(i) Then
				OutMess  "Проверка не пройдена на значении с индексом" & CStr(i)
				ReadFromMapTest = CBool(0)
			End If
		Next
	End If
	
	OutMess "Чтение окончено"

End Function

Public Function ReadRegistrsWithLongTypeFromMap(addr, nb, ft, bo, dt) As Variant
    Dim result As Variant
    
    result = GModbus1.modbusReadFromServer(addr, nb, ft, bo, dt)
    If (IsEmpty(result)) Then
        ReadRegistrsWithLongTypeFromMap = result
        Exit Function
    End If
    Dim resultforVB() As Variant
    ReDim resultforVB(0 To UBound(result), 0)
    Dim i As Integer
    
    For i = 0 To UBound(result)
        resultforVB(i, 0) = result(i)
    Next i
    
    ReadRegistrsWithLongTypeFromMap = resultforVB
End Function
```

### modbusRemoteMastersInitialization
```
int CGModbusCtrl::modbusRemoteMastersInitialization(int nb);
```
Для того, чтобы использовать технологию общения с remote Slaves, необходимо сначала установить соединения со всеми удалёнными slave.
Метод выделяет память для массива адаптеров, которые будут осуществлять протокол обмена информацией с remote Slaves в количестве nb.  

##### Параметры
- int nb   : количество соединений

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo

Пример использования: см описание функции modbusAddMasterToRemoteSlaveRTU.

### modbusAddMasterToRemoteSlaveRTU
```
int CGModbusCtrl::modbusAddMasterToRemoteSlaveRTU(BSTR device, int servId, int baud);
```
Метод добавляет в массив соединений RTU соединение с remote Slave по заданным параметрам.  

##### Параметры
- BSTR device : аргумент задает имя последовательного порта обрабатываемого операционной системой. В Windows необходимо добавить COM-имя с «\\.\» Для номера COM более 9, См. [MSDN] для получения более подробной информации.
- int servId  : id remote slave (см [wiki])
- int baud    : аргумент определяет скорость передачи сообщения, например. 9600, 19200, 57600, 115200 и т. Д.

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo
```
Function AddMasterToRemoteSlaveRTUTest()

	AddMasterToRemoteSlaveRTUTest = CBool(1)

	rc = Form.GModbus1.modbusRemoteMastersInitialization(1)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & "Не удалось проинициализировать Мастера Код ошибки: " & CStr(rc)
		AddMasterToRemoteSlaveRTUTest = CBool(0)
		Exit Function
	End If	
	
	OutMess  "Пробуем подключение к удалённому Slave по RTU"
	device = "\\.\COM15"
	servID = 17
	baud = 9600

	OutMess  "Параметры: device = " & CStr(device) 
	OutMess  "servID = " & CStr(servID)
	OutMess  "baud = " & CStr(baud)
		
	rc = Form.GModbus1.modbusAddMasterToRemoteSlaveRTU(device, servID, baud)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		AddMasterToRemoteSlaveRTUTest = CBool(0)
	End If	
	
End Function
```
### modbusAddMasterToRemoteSlaveTCP
```
int CGModbusCtrl::modbusAddMasterToRemoteSlaveTCP(BSTR ip, int port);
```
Метод добавляет в массив соединений TCP соединение с remote Slave по заданным параметрам.  

##### Параметры
- BSTR ip 	: ip(IPv4) адрес remote Slave
- int port  : порт, по которому будет происходить обмен данными (на устройстве remote Slave)

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo

Пример использования аналогичен примеру modbusAddMasterToRemoteSlaveRTU.

### modbusReadRegistersFromRemoteSlave
```
int CGModbusCtrl::modbusReadRegistersFromRemoteSlave(int addr, int nb, int rmtSlaveIndex);
```
Метод читает из remote Slave набор регистров, в количестве nb, начиная с адреса addr и записывает их в локальную карту регистров, синхронизируя тем самым значения на обеих картах  

##### Параметры
- int addr : адрес регистра в карте регистров, с которого начнётся чтение.
- int nb   : количество байт
- int rmtSlaveIndex : индекс remote Slave в массиве соединений

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo
```
Function ReadFromRemoteSalveTest()

	ReadFromRemoteSalveTest = CBool(1)
	OutMess "Переходим к чтению из Remote Salve"
	' *** Чтение значений из Remote Salve *** '
	addr = 0	'адрес регистра, с которого начинаем чтение
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	index = 0	'индекс соединения (в нашем случае оно одно, поэтому индекс 0)
	
	rc = Form.GModbus1.modbusReadRegistersFromRemoteSlave(addr, nb, index)
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		ReadFromRemoteSalveTest = CBool(0)
	End If	
	
	OutMess "Чтение окончено"

End Function
```
### modbusWriteRegistersToRemoteSlave
```
int CGModbusCtrl::modbusWriteRegistersToRemoteSlave(int addr, int nb, int rmtSlaveIndex);
```
Метод записывает на remote Slave набор регистров, в количестве nb, начиная с адреса addr. Значения берутся из локальной карты регистров, синхронизируя тем самым значения на обеих картах  

##### Параметры
- int addr : адрес регистра в карте регистров, с которого начнётся запись.
- int nb   : количество байт
- int rmtSlaveIndex : индекс remote Slave в массиве соединений

##### Возвращаемое значение
Возвращает код из перечисления ResultStatuses. В случае ошибок, доп. информацию можно получить вызовом CGModbusCtrl::getConnectionStatusInfo
```
Function WriteToRemoteServerTest()

	WriteToRemoteServerTest = CBool(1)
	OutMess "Переходим к записи на Remote Salve"
	' *** Запись значений в Remote Salve *** '
	addr = 0	'адрес регистра, с которого начинаем запись
	nb = 6 		'количество байт ( 3 регистра по 2 байта = 6)
	index = 0	'индекс соединения (в нашем случае оно одно, поэтому индекс 0)
	rc = Form.GModbus1.modbusWriteRegistersToRemoteSlave(addr, nb, index)
	If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		res = Form.InfoModbus()
		OutMess res & ". Код ошибки: " & CStr(rc)
		WriteToRemoteServerTest = CBool(0)
	End If
	OutMess "Запись окончена"
	
End Function
```
### modbusMastersUpdateConections
```
int CGModbusCtrl::modbusMastersUpdateConections();
```
Метод перебирает все адаптеры для remote Slaves и, в случае, отсутствия соединения восстанавливает его.


[wiki]: https://en.wikipedia.org/wiki/Modbus
[msdn]: Http://msdn.microsoft.com/en-us/library/aa365247(v=vs.85).aspx